<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];
    protected $appends = ['opts','opt_index'];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function getOptIndexAttribute()
    {
        return $this->attributes['opt_index'] = ["A", "B", "C", "D"];
    }

    public function getOptsAttribute()
    {
        return $this->attributes['opts'] = explode('|', $this->attributes['options']);
    }
}

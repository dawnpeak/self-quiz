<?php

namespace App\Admin\Controllers;

use App\Admin\Model\Question;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class QuestionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Question';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Question());
        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->equal('course.id','专业')->select(['1' => '软件工程','2' => '软件测试','3' => '软件项目管理','4' => '网络应用程序设计','5' => '多媒体','6' => '软件开发工具']);
            $filter->equal('type','类型')->select(['1' => '单选题','2' => '填空题','3' => '简答题']);
        });
        $grid->model()->orderBy('id','desc');
        $grid->column('id', __('Id'))->sortable();
        $grid->column('course.name', __('questions.Course id'));
        $grid->column('type', __('questions.Type'))->display(function ($type) {
            if ($type == 1) {
                return "<span class=\"label label-success\">单选题</span>";
            } elseif ($type == 2) {
                return "<span class=\"label label-info\">填空题</span>";
            } else {
                return "<span class=\"label label-warning\">简单题</span>";
            }
        });
        $grid->column('question', __('questions.Question'))->editable();
        $grid->column('answer', __('questions.Answer'));
        $grid->column('created_at', __('questions.Created at'));
        $grid->column('updated_at', __('questions.Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Question::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('course_id', __('Course id'));
        $show->field('question', __('Question'));
        $show->field('options', __('Options'));
        $show->field('answer', __('Answer'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Question());
        //$form->number('course_id', __('questions.Course id'));
        $form->select('course_id', __('questions.Course id'))->options('/admin/api/course');
        $form->radioButton('type', __('questions.Type'))
            ->options([
                1 => '单选',
                2 => '填空',
                3 => '简答',
            ])->when(1, function (Form $form) {
                $form->text('question', __('questions.Question'));
                $form->text('options', __('questions.Options'))->help('选项之间使用 | 分隔，如：A．功能需求|B．性能需求|C．可扩展性和灵活性|D．容错纠错能力');;
                $form->text('answer', __('questions.Answer'));
                $form->textarea('analysis', __('questions.Analysis'))->rows(8);
            })->when(2, function (Form $form) {
                $form->text('question', __('questions.Question'));
                $form->text('answer', __('questions.Answer'));
            })->when(3, function (Form $form) {
                $form->text('question', __('questions.Question'));
                $form->textarea('answer', __('questions.Answer'))->rows(10);
            });
        return $form;
    }
}


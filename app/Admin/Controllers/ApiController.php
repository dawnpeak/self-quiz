<?php

namespace App\Admin\Controllers;

use App\Course;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function course()
    {
       return Course::all(['id', 'name as text']);
    }
}

<?php

namespace App\Admin\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

//    public function setOptionsAttribute($value)
//    {
//        if (strpos($value, '|B') === false) {
//            $pattern                     = array('/B/', '/C/', '/D/');
//            $replacement                 = array('|B', '|C', '|D');
//            $res                         = preg_replace($pattern, $replacement, $value, 1);
//            $this->attributes['options'] = $res;
//        }
//    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Question;

class QuestionController extends Controller
{
    public function index()
    {
        return Question::inRandomOrder()->first();
    }
}

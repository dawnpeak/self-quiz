<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $contents = Question::orderBy('id', 'asc')->simplePaginate(1);
        return view('index', ['contents' => $contents]);
    }

    public function submit(Request $request)
    {
        $id     = $request->post('id');
        $answer = $request->post('answer');
        $info   = Question::whereId($id)->first(['answer','analysis']);
        return view('submit', ['answer' => $answer, 'info' => $info])->render();
    }

    public function answer($id)
    {
        $content = Question::whereId($id)->first();
        if (!$content) {
            abort(404);
        }
        return view('index', ['contents' => $content]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Course;
use App\Question;

class CourseController extends Controller
{
    public function index($id)
    {
        $course = Course::find($id);
        return view('cate', ['courseid' => $course->id, 'coursename' => $course->name]);
    }

    public function question($id, $cateid)
    {
        $contents  = Question::where(['course_id' => $id, 'type' => $cateid])->orderBy('id', 'desc')->simplePaginate(1);
        $count     = count($contents->toarray()['data']);
        $course    = Course::find($id);
        $cateArray = [
            1 => '单选题',
            2 => '填空题',
            3 => '简答题',
        ];
        $data      = [
            'contents'   => $contents,
            'coursename' => $course->name,
            'courseid'   => $course->id,
            'count'      => $count,
            'catename'   => $cateArray[$cateid]
        ];
        if ($cateid == 3) {
            return view('shortanswer', $data);
        } else {
            return view('question', $data);
        }
    }
}

**刷题库**

模拟刷题，自我测验。包括选择，填空，简答题，基于laravel7搭建,服务器要求参照[laravel文档](https://learnku.com/docs/laravel/7.x/installation/7447#server-requirements)。

![演示截图](https://gitee.com/dawnpeak/self-quiz/raw/master/demo.png "演示截图")

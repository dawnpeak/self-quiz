/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.10.203
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 192.168.10.203:3306
 Source Schema         : selfquiz

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 07/09/2020 10:38:44
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `permission` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES (1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, NULL, NULL);
INSERT INTO `admin_menu` VALUES (2, 0, 5, 'Admin', 'fa-tasks', '', NULL, NULL, '2020-08-24 02:43:38');
INSERT INTO `admin_menu` VALUES (3, 2, 6, 'Users', 'fa-users', 'auth/users', NULL, NULL, '2020-08-24 02:43:38');
INSERT INTO `admin_menu` VALUES (4, 2, 7, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, '2020-08-24 02:43:38');
INSERT INTO `admin_menu` VALUES (5, 2, 8, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, '2020-08-24 02:43:38');
INSERT INTO `admin_menu` VALUES (6, 2, 9, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, '2020-08-24 02:43:38');
INSERT INTO `admin_menu` VALUES (7, 2, 10, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, '2020-08-24 02:43:38');
INSERT INTO `admin_menu` VALUES (8, 0, 3, '课程管理', 'fa-bars', 'courses', '*', '2020-08-24 02:41:54', '2020-08-24 02:43:43');
INSERT INTO `admin_menu` VALUES (9, 0, 4, '题目管理', 'fa-bars', 'questions', NULL, '2020-08-24 02:42:27', '2020-08-24 02:43:43');
INSERT INTO `admin_menu` VALUES (10, 0, 2, '专业管理', 'fa-bars', 'majors', NULL, '2020-08-24 02:43:06', '2020-08-24 02:43:38');

-- ----------------------------
-- Table structure for admin_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE `admin_operation_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `input` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `admin_operation_log_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 472 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_operation_log
-- ----------------------------
INSERT INTO `admin_operation_log` VALUES (1, 1, 'admin/auth/login', 'GET', '192.168.1.100', '[]', '2020-07-22 03:52:43', '2020-07-22 03:52:43');
INSERT INTO `admin_operation_log` VALUES (2, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-07-22 03:52:44', '2020-07-22 03:52:44');
INSERT INTO `admin_operation_log` VALUES (3, 1, 'admin/majors/create', 'GET', '192.168.1.100', '[]', '2020-07-22 03:53:05', '2020-07-22 03:53:05');
INSERT INTO `admin_operation_log` VALUES (4, 1, 'admin/majors', 'GET', '192.168.1.100', '[]', '2020-07-22 03:53:09', '2020-07-22 03:53:09');
INSERT INTO `admin_operation_log` VALUES (5, 1, 'admin/majors/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-07-22 03:53:13', '2020-07-22 03:53:13');
INSERT INTO `admin_operation_log` VALUES (6, 1, 'admin/majors', 'POST', '192.168.1.100', '{\"code\":\"080720\",\"name\":\"\\u8f6f\\u4ef6\\u5de5\\u7a0b [\\u72ec\\u7acb\\u672c\\u79d1\\u6bb5]\",\"_token\":\"8lMhoqNcsHNYVv4534PPSNSJjQjW5M88ZjUylgU3\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/majors\\/\"}', '2020-07-22 03:53:18', '2020-07-22 03:53:18');
INSERT INTO `admin_operation_log` VALUES (7, 1, 'admin/majors', 'GET', '192.168.1.100', '[]', '2020-07-22 03:53:19', '2020-07-22 03:53:19');
INSERT INTO `admin_operation_log` VALUES (8, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-07-22 03:54:15', '2020-07-22 03:54:15');
INSERT INTO `admin_operation_log` VALUES (9, 1, 'admin/courses/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-07-22 03:54:19', '2020-07-22 03:54:19');
INSERT INTO `admin_operation_log` VALUES (10, 1, 'admin/courses/create', 'GET', '192.168.1.100', '[]', '2020-07-22 03:54:40', '2020-07-22 03:54:40');
INSERT INTO `admin_operation_log` VALUES (11, 1, 'admin/courses', 'POST', '192.168.1.100', '{\"major_id\":\"1\",\"code\":\"02333\",\"name\":\"\\u8f6f\\u4ef6\\u5de5\\u7a0b\",\"_token\":\"8lMhoqNcsHNYVv4534PPSNSJjQjW5M88ZjUylgU3\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/courses\"}', '2020-07-22 03:55:37', '2020-07-22 03:55:37');
INSERT INTO `admin_operation_log` VALUES (12, 1, 'admin/courses/create', 'GET', '192.168.1.100', '[]', '2020-07-22 03:55:38', '2020-07-22 03:55:38');
INSERT INTO `admin_operation_log` VALUES (13, 1, 'admin/courses', 'POST', '192.168.1.100', '{\"major_id\":\"1\",\"code\":\"02333\",\"name\":\"\\u8f6f\\u4ef6\\u5de5\\u7a0b\",\"_token\":\"8lMhoqNcsHNYVv4534PPSNSJjQjW5M88ZjUylgU3\"}', '2020-07-22 03:56:18', '2020-07-22 03:56:18');
INSERT INTO `admin_operation_log` VALUES (14, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-07-22 03:56:19', '2020-07-22 03:56:19');
INSERT INTO `admin_operation_log` VALUES (15, 1, 'admin/majors', 'GET', '192.168.1.100', '[]', '2020-07-22 03:56:29', '2020-07-22 03:56:29');
INSERT INTO `admin_operation_log` VALUES (16, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-07-22 03:57:17', '2020-07-22 03:57:17');
INSERT INTO `admin_operation_log` VALUES (17, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-07-22 04:01:53', '2020-07-22 04:01:53');
INSERT INTO `admin_operation_log` VALUES (18, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-07-22 04:02:11', '2020-07-22 04:02:11');
INSERT INTO `admin_operation_log` VALUES (19, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-14 08:27:01', '2020-08-14 08:27:01');
INSERT INTO `admin_operation_log` VALUES (20, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-14 08:27:15', '2020-08-14 08:27:15');
INSERT INTO `admin_operation_log` VALUES (21, 1, 'admin/majors', 'GET', '192.168.1.100', '[]', '2020-08-14 08:27:45', '2020-08-14 08:27:45');
INSERT INTO `admin_operation_log` VALUES (22, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 08:28:00', '2020-08-14 08:28:00');
INSERT INTO `admin_operation_log` VALUES (23, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-14 08:28:04', '2020-08-14 08:28:04');
INSERT INTO `admin_operation_log` VALUES (24, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:31:43', '2020-08-14 08:31:43');
INSERT INTO `admin_operation_log` VALUES (25, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:33:09', '2020-08-14 08:33:09');
INSERT INTO `admin_operation_log` VALUES (26, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:33:43', '2020-08-14 08:33:43');
INSERT INTO `admin_operation_log` VALUES (27, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:34:03', '2020-08-14 08:34:03');
INSERT INTO `admin_operation_log` VALUES (28, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:36:03', '2020-08-14 08:36:03');
INSERT INTO `admin_operation_log` VALUES (29, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:36:21', '2020-08-14 08:36:21');
INSERT INTO `admin_operation_log` VALUES (30, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:36:39', '2020-08-14 08:36:39');
INSERT INTO `admin_operation_log` VALUES (31, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:36:50', '2020-08-14 08:36:50');
INSERT INTO `admin_operation_log` VALUES (32, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:41:13', '2020-08-14 08:41:13');
INSERT INTO `admin_operation_log` VALUES (33, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:41:58', '2020-08-14 08:41:58');
INSERT INTO `admin_operation_log` VALUES (34, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:42:02', '2020-08-14 08:42:02');
INSERT INTO `admin_operation_log` VALUES (35, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:43:15', '2020-08-14 08:43:15');
INSERT INTO `admin_operation_log` VALUES (36, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:43:36', '2020-08-14 08:43:36');
INSERT INTO `admin_operation_log` VALUES (37, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:43:55', '2020-08-14 08:43:55');
INSERT INTO `admin_operation_log` VALUES (38, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 08:44:06', '2020-08-14 08:44:06');
INSERT INTO `admin_operation_log` VALUES (39, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 08:45:18', '2020-08-14 08:45:18');
INSERT INTO `admin_operation_log` VALUES (40, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-14 08:45:28', '2020-08-14 08:45:28');
INSERT INTO `admin_operation_log` VALUES (41, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:47:07', '2020-08-14 08:47:07');
INSERT INTO `admin_operation_log` VALUES (42, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:47:40', '2020-08-14 08:47:40');
INSERT INTO `admin_operation_log` VALUES (43, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:48:47', '2020-08-14 08:48:47');
INSERT INTO `admin_operation_log` VALUES (44, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:51:07', '2020-08-14 08:51:07');
INSERT INTO `admin_operation_log` VALUES (45, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:54:21', '2020-08-14 08:54:21');
INSERT INTO `admin_operation_log` VALUES (46, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:56:53', '2020-08-14 08:56:53');
INSERT INTO `admin_operation_log` VALUES (47, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 08:57:23', '2020-08-14 08:57:23');
INSERT INTO `admin_operation_log` VALUES (48, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 08:58:17', '2020-08-14 08:58:17');
INSERT INTO `admin_operation_log` VALUES (49, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:00:44', '2020-08-14 09:00:44');
INSERT INTO `admin_operation_log` VALUES (50, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:01:52', '2020-08-14 09:01:52');
INSERT INTO `admin_operation_log` VALUES (51, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:07:59', '2020-08-14 09:07:59');
INSERT INTO `admin_operation_log` VALUES (52, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:08:07', '2020-08-14 09:08:07');
INSERT INTO `admin_operation_log` VALUES (53, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:08:20', '2020-08-14 09:08:20');
INSERT INTO `admin_operation_log` VALUES (54, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:08:45', '2020-08-14 09:08:45');
INSERT INTO `admin_operation_log` VALUES (55, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:10:00', '2020-08-14 09:10:00');
INSERT INTO `admin_operation_log` VALUES (56, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:12:20', '2020-08-14 09:12:20');
INSERT INTO `admin_operation_log` VALUES (57, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:12:58', '2020-08-14 09:12:58');
INSERT INTO `admin_operation_log` VALUES (58, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:13:24', '2020-08-14 09:13:24');
INSERT INTO `admin_operation_log` VALUES (59, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:13:35', '2020-08-14 09:13:35');
INSERT INTO `admin_operation_log` VALUES (60, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:14:32', '2020-08-14 09:14:32');
INSERT INTO `admin_operation_log` VALUES (61, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-14 09:15:00', '2020-08-14 09:15:00');
INSERT INTO `admin_operation_log` VALUES (62, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-14 09:15:25', '2020-08-14 09:15:25');
INSERT INTO `admin_operation_log` VALUES (63, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:15:26', '2020-08-14 09:15:26');
INSERT INTO `admin_operation_log` VALUES (64, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"\\u8f6f\\u4ef6\\u662f\\u4e00\\u79cd\\u903b\\u8f91\\u4ea7\\u54c1\\uff0c\\u5b83\\u7684\\u5f00\\u53d1\\u4e3b\\u8981\\u662f\\uff08\\uff09\",\"options\":{\"new_1\":{\"\\u9009\\u9879\":\"A\",\"\\u5185\\u5bb9\":\"\\u7814\\u5236\",\"_remove_\":\"0\"},\"new_2\":{\"\\u9009\\u9879\":\"B\",\"\\u5185\\u5bb9\":\"\\u62f7\\u8d1d\",\"_remove_\":\"0\"},\"new_3\":{\"\\u9009\\u9879\":\"C\",\"\\u5185\\u5bb9\":\"\\u518d\\u751f\\u4ea7\",\"_remove_\":\"0\"},\"new_4\":{\"\\u9009\\u9879\":\"D\",\"\\u5185\\u5bb9\":\"\\u590d\\u5236\",\"_remove_\":\"0\"}},\"answer\":\"A\",\"_token\":\"5ZJnDkEvyfrLsEaZs6P5D1iEHnsf9xjfRNxixOJq\"}', '2020-08-14 09:16:26', '2020-08-14 09:16:26');
INSERT INTO `admin_operation_log` VALUES (65, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 09:16:27', '2020-08-14 09:16:27');
INSERT INTO `admin_operation_log` VALUES (66, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-14 09:20:44', '2020-08-14 09:20:44');
INSERT INTO `admin_operation_log` VALUES (67, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:20:45', '2020-08-14 09:20:45');
INSERT INTO `admin_operation_log` VALUES (68, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"\\u8f6f\\u4ef6\\u751f\\u547d\\u5468\\u671f\\u4e00\\u822c\\u5305\\u62ec\\uff1a\\u8f6f\\u4ef6\\u5f00\\u53d1\\u671f\\u548c\\u8f6f\\u4ef6\\u8fd0\\u884c\\u671f\\uff0c\\u4e0b\\u8ff0\\uff08\\uff09\\u4e0d\\u662f\\u8f6f\\u4ef6\\u5f00\\u53d1\\u671f\\u6240\\u5e94\\u5305\\u542b\\u7684\\u5185\\u5bb9\\u3002\",\"options\":{\"new_1\":{\"\\u9009\\u9879\":\"A\",\"\\u5185\\u5bb9\":\"\\u9700\\u6c42\\u5206\\u6790\",\"_remove_\":\"0\"},\"new_2\":{\"\\u9009\\u9879\":\"B\",\"\\u5185\\u5bb9\":\"\\u7ed3\\u6784\\u8bbe\\u8ba1\",\"_remove_\":\"0\"},\"new_3\":{\"\\u9009\\u9879\":\"C\",\"\\u5185\\u5bb9\":\"\\u7a0b\\u5e8f\\u7f16\\u5236\",\"_remove_\":\"0\"},\"new_4\":{\"\\u9009\\u9879\":\"D\",\"\\u5185\\u5bb9\":\"\\u8f6f\\u4ef6\\u7ef4\\u62a4\",\"_remove_\":\"0\"}},\"answer\":\"D\",\"_token\":\"5ZJnDkEvyfrLsEaZs6P5D1iEHnsf9xjfRNxixOJq\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-14 09:21:43', '2020-08-14 09:21:43');
INSERT INTO `admin_operation_log` VALUES (69, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 09:21:44', '2020-08-14 09:21:44');
INSERT INTO `admin_operation_log` VALUES (70, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 09:23:59', '2020-08-14 09:23:59');
INSERT INTO `admin_operation_log` VALUES (71, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-14 09:24:01', '2020-08-14 09:24:01');
INSERT INTO `admin_operation_log` VALUES (72, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:24:02', '2020-08-14 09:24:02');
INSERT INTO `admin_operation_log` VALUES (73, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"123\",\"options\":{\"new_1\":{\"\\u9009\\u9879\":\"1\",\"\\u5185\\u5bb9\":\"12312\",\"_remove_\":\"0\"},\"new_2\":{\"\\u9009\\u9879\":\"2\",\"\\u5185\\u5bb9\":\"123123123\",\"_remove_\":\"0\"},\"new_3\":{\"\\u9009\\u9879\":\"3\",\"\\u5185\\u5bb9\":\"12312\",\"_remove_\":\"0\"}},\"answer\":\"2\",\"_token\":\"5ZJnDkEvyfrLsEaZs6P5D1iEHnsf9xjfRNxixOJq\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-14 09:24:22', '2020-08-14 09:24:22');
INSERT INTO `admin_operation_log` VALUES (74, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-14 09:24:22', '2020-08-14 09:24:22');
INSERT INTO `admin_operation_log` VALUES (75, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-14 09:24:55', '2020-08-14 09:24:55');
INSERT INTO `admin_operation_log` VALUES (76, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-15 09:20:27', '2020-08-15 09:20:27');
INSERT INTO `admin_operation_log` VALUES (77, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-17 01:11:47', '2020-08-17 01:11:47');
INSERT INTO `admin_operation_log` VALUES (78, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-17 02:42:09', '2020-08-17 02:42:09');
INSERT INTO `admin_operation_log` VALUES (79, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-17 07:36:24', '2020-08-17 07:36:24');
INSERT INTO `admin_operation_log` VALUES (80, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-19 09:21:27', '2020-08-19 09:21:27');
INSERT INTO `admin_operation_log` VALUES (81, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-19 09:21:37', '2020-08-19 09:21:37');
INSERT INTO `admin_operation_log` VALUES (82, 1, 'admin/questions/1/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-19 09:21:44', '2020-08-19 09:21:44');
INSERT INTO `admin_operation_log` VALUES (83, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-19 09:21:46', '2020-08-19 09:21:46');
INSERT INTO `admin_operation_log` VALUES (84, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-19 09:22:49', '2020-08-19 09:22:49');
INSERT INTO `admin_operation_log` VALUES (85, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-19 09:25:26', '2020-08-19 09:25:26');
INSERT INTO `admin_operation_log` VALUES (86, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-19 09:38:03', '2020-08-19 09:38:03');
INSERT INTO `admin_operation_log` VALUES (87, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-20 09:14:21', '2020-08-20 09:14:21');
INSERT INTO `admin_operation_log` VALUES (88, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-20 09:14:31', '2020-08-20 09:14:31');
INSERT INTO `admin_operation_log` VALUES (89, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-20 09:14:48', '2020-08-20 09:14:48');
INSERT INTO `admin_operation_log` VALUES (90, 1, 'admin/questions/3/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-20 09:18:43', '2020-08-20 09:18:43');
INSERT INTO `admin_operation_log` VALUES (91, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-20 09:18:44', '2020-08-20 09:18:44');
INSERT INTO `admin_operation_log` VALUES (92, 1, 'admin/questions/3', 'PUT', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"\\u4ee5\\u6587\\u6863\\u4f5c\\u4e3a\\u9a71\\u52a8\\uff0c\\u9002\\u5408\\u4e8e\\u8f6f\\u4ef6\\u9700\\u6c42\\u5f88\\u660e\\u786e\\u7684\\u8f6f\\u4ef6\\u9879\\u76ee\\u7684\\u751f\\u5b58\\u5468\\u671f\\u6a21\\u578b\\u662f\\uff08\\uff09\\u3002\",\"options\":{\"0\":{\"key\":\"A\",\"value\":\"\\u55b7\\u6cc9\\u6a21\\u578b\",\"_remove_\":\"0\"},\"1\":{\"key\":\"B\",\"value\":\"\\u589e\\u91cf\\u6a21\\u578b\",\"_remove_\":\"0\"},\"2\":{\"key\":\"C\",\"value\":\"\\u7011\\u5e03\\u6a21\\u578b\",\"_remove_\":\"0\"},\"new_1\":{\"key\":\"D\",\"value\":\"\\u87ba\\u65cb\\u6a21\\u578b\",\"_remove_\":\"0\"}},\"answer\":\"C\",\"analysis\":null,\"_token\":\"EY0INn0SGjbp6PCfwzBrX0iOUG07sNkNktD2opaa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-20 09:19:50', '2020-08-20 09:19:50');
INSERT INTO `admin_operation_log` VALUES (93, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-20 09:19:51', '2020-08-20 09:19:51');
INSERT INTO `admin_operation_log` VALUES (94, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-20 09:20:07', '2020-08-20 09:20:07');
INSERT INTO `admin_operation_log` VALUES (95, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-20 09:20:08', '2020-08-20 09:20:08');
INSERT INTO `admin_operation_log` VALUES (96, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"\\u5728\\u8f6f\\u4ef6\\u751f\\u5b58\\u5468\\u671f\\u4e2d\\uff0c\\uff08\\uff09\\u9636\\u6bb5\\u5fc5\\u987b\\u8981\\u56de\\u7b54\\u7684\\u95ee\\u9898\\u662f\\u201c\\u8981\\u89e3\\u51b3\\u7684\\u95ee\\u9898\\u662f\\u505a\\u4ec0\\u4e48\\uff1f\",\"options\":{\"new_1\":{\"key\":\"A\",\"value\":\"\\u8be6\\u7ec6\\u8bbe\\u8ba1\",\"_remove_\":\"0\"},\"new_2\":{\"key\":\"B\",\"value\":\"\\u53ef\\u884c\\u6027\\u5206\\u6790\\u548c\\u9879\\u76ee\\u5f00\\u53d1\\u8ba1\\u5212\",\"_remove_\":\"0\"},\"new_3\":{\"key\":\"C\",\"value\":\"\\u6982\\u8981\\u8bbe\\u8ba1\",\"_remove_\":\"0\"},\"new_4\":{\"key\":\"D\",\"value\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\",\"_remove_\":\"0\"}},\"answer\":\"B\",\"analysis\":null,\"_token\":\"EY0INn0SGjbp6PCfwzBrX0iOUG07sNkNktD2opaa\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-20 09:23:20', '2020-08-20 09:23:20');
INSERT INTO `admin_operation_log` VALUES (97, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-20 09:23:20', '2020-08-20 09:23:20');
INSERT INTO `admin_operation_log` VALUES (98, 1, 'admin/questions/4/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-20 09:23:26', '2020-08-20 09:23:26');
INSERT INTO `admin_operation_log` VALUES (99, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-20 09:23:27', '2020-08-20 09:23:27');
INSERT INTO `admin_operation_log` VALUES (100, 1, 'admin/questions/4', 'PUT', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"\\u5728\\u8f6f\\u4ef6\\u751f\\u5b58\\u5468\\u671f\\u4e2d\\uff0c\\uff08\\uff09\\u9636\\u6bb5\\u5fc5\\u987b\\u8981\\u56de\\u7b54\\u7684\\u95ee\\u9898\\u662f\\u201c\\u8981\\u89e3\\u51b3\\u7684\\u95ee\\u9898\\u662f\\u505a\\u4ec0\\u4e48\\uff1f\\\"\",\"options\":[{\"key\":\"A\",\"value\":\"\\u8be6\\u7ec6\\u8bbe\\u8ba1\",\"_remove_\":\"0\"},{\"key\":\"B\",\"value\":\"\\u53ef\\u884c\\u6027\\u5206\\u6790\\u548c\\u9879\\u76ee\\u5f00\\u53d1\\u8ba1\\u5212\",\"_remove_\":\"0\"},{\"key\":\"C\",\"value\":\"\\u6982\\u8981\\u8bbe\\u8ba1\",\"_remove_\":\"0\"},{\"key\":\"D\",\"value\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\",\"_remove_\":\"0\"}],\"answer\":\"B\",\"analysis\":null,\"_token\":\"EY0INn0SGjbp6PCfwzBrX0iOUG07sNkNktD2opaa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-20 09:23:34', '2020-08-20 09:23:34');
INSERT INTO `admin_operation_log` VALUES (101, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-20 09:23:34', '2020-08-20 09:23:34');
INSERT INTO `admin_operation_log` VALUES (102, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-24 02:19:10', '2020-08-24 02:19:10');
INSERT INTO `admin_operation_log` VALUES (103, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:19:20', '2020-08-24 02:19:20');
INSERT INTO `admin_operation_log` VALUES (104, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:23:17', '2020-08-24 02:23:17');
INSERT INTO `admin_operation_log` VALUES (105, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:24:05', '2020-08-24 02:24:05');
INSERT INTO `admin_operation_log` VALUES (106, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:24:35', '2020-08-24 02:24:35');
INSERT INTO `admin_operation_log` VALUES (107, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:26:10', '2020-08-24 02:26:10');
INSERT INTO `admin_operation_log` VALUES (108, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:26:52', '2020-08-24 02:26:52');
INSERT INTO `admin_operation_log` VALUES (109, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 02:29:23', '2020-08-24 02:29:23');
INSERT INTO `admin_operation_log` VALUES (110, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:29:29', '2020-08-24 02:29:29');
INSERT INTO `admin_operation_log` VALUES (111, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"3\"}', '2020-08-24 02:29:32', '2020-08-24 02:29:32');
INSERT INTO `admin_operation_log` VALUES (112, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"3\"}', '2020-08-24 02:31:06', '2020-08-24 02:31:06');
INSERT INTO `admin_operation_log` VALUES (113, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"3\"}', '2020-08-24 02:36:05', '2020-08-24 02:36:05');
INSERT INTO `admin_operation_log` VALUES (114, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"3\"}', '2020-08-24 02:36:08', '2020-08-24 02:36:08');
INSERT INTO `admin_operation_log` VALUES (115, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"3\"}', '2020-08-24 02:36:30', '2020-08-24 02:36:30');
INSERT INTO `admin_operation_log` VALUES (116, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:36:54', '2020-08-24 02:36:54');
INSERT INTO `admin_operation_log` VALUES (117, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2020-08-24 02:36:59', '2020-08-24 02:36:59');
INSERT INTO `admin_operation_log` VALUES (118, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:40:56', '2020-08-24 02:40:56');
INSERT INTO `admin_operation_log` VALUES (119, 1, 'admin/auth/menu', 'POST', '192.168.1.100', '{\"parent_id\":\"0\",\"title\":\"\\u8bfe\\u7a0b\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"courses\",\"roles\":[null],\"permission\":\"*\",\"_token\":\"7deV29ZerhIsHYzRZLLYpI2brIZqui47uGJJOsLz\"}', '2020-08-24 02:41:54', '2020-08-24 02:41:54');
INSERT INTO `admin_operation_log` VALUES (120, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:41:55', '2020-08-24 02:41:55');
INSERT INTO `admin_operation_log` VALUES (121, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:42:03', '2020-08-24 02:42:03');
INSERT INTO `admin_operation_log` VALUES (122, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:42:05', '2020-08-24 02:42:05');
INSERT INTO `admin_operation_log` VALUES (123, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:42:14', '2020-08-24 02:42:14');
INSERT INTO `admin_operation_log` VALUES (124, 1, 'admin/auth/menu', 'POST', '192.168.1.100', '{\"parent_id\":\"0\",\"title\":\"\\u9898\\u76ee\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"questions\",\"roles\":[null],\"permission\":null,\"_token\":\"7deV29ZerhIsHYzRZLLYpI2brIZqui47uGJJOsLz\"}', '2020-08-24 02:42:27', '2020-08-24 02:42:27');
INSERT INTO `admin_operation_log` VALUES (125, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:42:28', '2020-08-24 02:42:28');
INSERT INTO `admin_operation_log` VALUES (126, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:42:31', '2020-08-24 02:42:31');
INSERT INTO `admin_operation_log` VALUES (127, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:42:33', '2020-08-24 02:42:33');
INSERT INTO `admin_operation_log` VALUES (128, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:42:43', '2020-08-24 02:42:43');
INSERT INTO `admin_operation_log` VALUES (129, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:42:55', '2020-08-24 02:42:55');
INSERT INTO `admin_operation_log` VALUES (130, 1, 'admin/auth/menu', 'POST', '192.168.1.100', '{\"parent_id\":\"0\",\"title\":\"\\u4e13\\u4e1a\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"majors\",\"roles\":[null],\"permission\":null,\"_token\":\"7deV29ZerhIsHYzRZLLYpI2brIZqui47uGJJOsLz\"}', '2020-08-24 02:43:06', '2020-08-24 02:43:06');
INSERT INTO `admin_operation_log` VALUES (131, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:43:07', '2020-08-24 02:43:07');
INSERT INTO `admin_operation_log` VALUES (132, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:43:12', '2020-08-24 02:43:12');
INSERT INTO `admin_operation_log` VALUES (133, 1, 'admin/majors', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:14', '2020-08-24 02:43:14');
INSERT INTO `admin_operation_log` VALUES (134, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:24', '2020-08-24 02:43:24');
INSERT INTO `admin_operation_log` VALUES (135, 1, 'admin/auth/menu', 'POST', '192.168.1.100', '{\"_token\":\"7deV29ZerhIsHYzRZLLYpI2brIZqui47uGJJOsLz\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":10},{\\\"id\\\":9},{\\\"id\\\":8},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]}]\"}', '2020-08-24 02:43:37', '2020-08-24 02:43:37');
INSERT INTO `admin_operation_log` VALUES (136, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:38', '2020-08-24 02:43:38');
INSERT INTO `admin_operation_log` VALUES (137, 1, 'admin/auth/menu', 'POST', '192.168.1.100', '{\"_token\":\"7deV29ZerhIsHYzRZLLYpI2brIZqui47uGJJOsLz\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":10},{\\\"id\\\":8},{\\\"id\\\":9},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]}]\"}', '2020-08-24 02:43:43', '2020-08-24 02:43:43');
INSERT INTO `admin_operation_log` VALUES (138, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:43', '2020-08-24 02:43:43');
INSERT INTO `admin_operation_log` VALUES (139, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:45', '2020-08-24 02:43:45');
INSERT INTO `admin_operation_log` VALUES (140, 1, 'admin/auth/menu', 'GET', '192.168.1.100', '[]', '2020-08-24 02:43:47', '2020-08-24 02:43:47');
INSERT INTO `admin_operation_log` VALUES (141, 1, 'admin/majors', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:49', '2020-08-24 02:43:49');
INSERT INTO `admin_operation_log` VALUES (142, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:51', '2020-08-24 02:43:51');
INSERT INTO `admin_operation_log` VALUES (143, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 02:43:57', '2020-08-24 02:43:57');
INSERT INTO `admin_operation_log` VALUES (144, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-24 02:43:58', '2020-08-24 02:43:58');
INSERT INTO `admin_operation_log` VALUES (145, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 03:59:44', '2020-08-24 03:59:44');
INSERT INTO `admin_operation_log` VALUES (146, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 03:59:48', '2020-08-24 03:59:48');
INSERT INTO `admin_operation_log` VALUES (147, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 04:43:02', '2020-08-24 04:43:02');
INSERT INTO `admin_operation_log` VALUES (148, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 05:36:02', '2020-08-24 05:36:02');
INSERT INTO `admin_operation_log` VALUES (149, 1, 'admin/questions/9/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 05:39:18', '2020-08-24 05:39:18');
INSERT INTO `admin_operation_log` VALUES (150, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-24 05:39:19', '2020-08-24 05:39:19');
INSERT INTO `admin_operation_log` VALUES (151, 1, 'admin/questions/9', 'PUT', '192.168.1.100', '{\"course_id\":\"1\",\"question\":\"\\u4e0b\\u5217\\u8f6f\\u4ef6\\u5f00\\u53d1\\u6a21\\u578b\\u4e2d\\uff0c\\u4ee5\\u9762\\u5411\\u5bf9\\u8c61\\u7684\\u8f6f\\u4ef6\\u5f00\\u53d1\\u65b9\\u6cd5\\u4e3a\\u57fa\\u7840\\uff0c\\u4ee5\\u7528\\u6237\\u7684\\u9700\\u6c42\\u4e3a\\u52a8\\u529b\\uff0c\\u4ee5\\u5bf9\\u8c61\\u6765\\u9a71\\u52a8\\u7684\\u6a21\\u578b\\u662f\\uff08 \\uff09\\u3002\",\"options\":\"A\\uff0c\\u539f\\u578b\\u6a21\\u578b|B\\uff0c\\u7011\\u5e03\\u6a21\\u578b|C\\uff0c\\u55b7\\u6cc9\\u6a21\\u578b|D\\uff0c\\u87ba\\u65cb\\u6a21\\u578b\",\"answer\":\"C\",\"analysis\":\"\\u6682\\u65e0\",\"_token\":\"7deV29ZerhIsHYzRZLLYpI2brIZqui47uGJJOsLz\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-24 05:39:36', '2020-08-24 05:39:36');
INSERT INTO `admin_operation_log` VALUES (152, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-24 05:39:36', '2020-08-24 05:39:36');
INSERT INTO `admin_operation_log` VALUES (153, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-24 08:18:17', '2020-08-24 08:18:17');
INSERT INTO `admin_operation_log` VALUES (154, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-25 03:46:28', '2020-08-25 03:46:28');
INSERT INTO `admin_operation_log` VALUES (155, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-25 03:46:28', '2020-08-25 03:46:28');
INSERT INTO `admin_operation_log` VALUES (156, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-25 03:46:54', '2020-08-25 03:46:54');
INSERT INTO `admin_operation_log` VALUES (157, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:46:54', '2020-08-25 03:46:54');
INSERT INTO `admin_operation_log` VALUES (158, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:46:54', '2020-08-25 03:46:54');
INSERT INTO `admin_operation_log` VALUES (159, 1, 'admin/majors', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:47:20', '2020-08-25 03:47:20');
INSERT INTO `admin_operation_log` VALUES (160, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:47:21', '2020-08-25 03:47:21');
INSERT INTO `admin_operation_log` VALUES (161, 1, 'admin/courses/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:47:24', '2020-08-25 03:47:24');
INSERT INTO `admin_operation_log` VALUES (162, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:47:47', '2020-08-25 03:47:47');
INSERT INTO `admin_operation_log` VALUES (163, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:47:51', '2020-08-25 03:47:51');
INSERT INTO `admin_operation_log` VALUES (164, 1, 'admin/courses/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:47:52', '2020-08-25 03:47:52');
INSERT INTO `admin_operation_log` VALUES (165, 1, 'admin/courses', 'POST', '192.168.1.100', '{\"major_id\":\"1\",\"code\":\"07028\",\"name\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u6280\\u672f\",\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/courses\"}', '2020-08-25 03:48:06', '2020-08-25 03:48:06');
INSERT INTO `admin_operation_log` VALUES (166, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-08-25 03:48:07', '2020-08-25 03:48:07');
INSERT INTO `admin_operation_log` VALUES (167, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:48:09', '2020-08-25 03:48:09');
INSERT INTO `admin_operation_log` VALUES (168, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:48:13', '2020-08-25 03:48:13');
INSERT INTO `admin_operation_log` VALUES (169, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 03:48:13', '2020-08-25 03:48:13');
INSERT INTO `admin_operation_log` VALUES (170, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u8f6f\\u4ef6\\u5c5e\\u6027\\u4e2d\\uff0c7\\u8f6f\\u4ef6\\u4ea7\\u54c1\\u9996\\u8981\\u6ee1\\u8db3\\u7684\\u5e94\\u8be5\\u662f\\uff08\\uff09\",\"options\":\"A\\uff0e\\u529f\\u80fd\\u9700\\u6c42|B\\uff0e\\u6027\\u80fd\\u9700\\u6c42|C\\uff0e\\u53ef\\u6269\\u5c55\\u6027\\u548c\\u7075\\u6d3b\\u6027|D\\uff0e\\u5bb9\\u9519\\u7ea0\\u9519\\u80fd\\u529b\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-25 03:49:15', '2020-08-25 03:49:15');
INSERT INTO `admin_operation_log` VALUES (171, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 03:49:16', '2020-08-25 03:49:16');
INSERT INTO `admin_operation_log` VALUES (172, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:49:24', '2020-08-25 03:49:24');
INSERT INTO `admin_operation_log` VALUES (173, 1, 'admin/questions/70/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:49:57', '2020-08-25 03:49:57');
INSERT INTO `admin_operation_log` VALUES (174, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 03:49:58', '2020-08-25 03:49:58');
INSERT INTO `admin_operation_log` VALUES (175, 1, 'admin/questions/70', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u8f6f\\u4ef6\\u5c5e\\u6027\\u4e2d\\uff0c\\u8f6f\\u4ef6\\u4ea7\\u54c1\\u9996\\u8981\\u6ee1\\u8db3\\u7684\\u5e94\\u8be5\\u662f\\uff08\\uff09\",\"options\":\"A\\uff0e\\u529f\\u80fd\\u9700\\u6c42|B\\uff0e\\u6027\\u80fd\\u9700\\u6c42|C\\uff0e\\u53ef\\u6269\\u5c55\\u6027\\u548c\\u7075\\u6d3b\\u6027|D\\uff0e\\u5bb9\\u9519\\u7ea0\\u9519\\u80fd\\u529b\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 03:50:03', '2020-08-25 03:50:03');
INSERT INTO `admin_operation_log` VALUES (176, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 03:50:03', '2020-08-25 03:50:03');
INSERT INTO `admin_operation_log` VALUES (177, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 03:50:23', '2020-08-25 03:50:23');
INSERT INTO `admin_operation_log` VALUES (178, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 03:50:23', '2020-08-25 03:50:23');
INSERT INTO `admin_operation_log` VALUES (179, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u7f3a\\u9677\\u4ea7\\u751f\\u7684\\u539f\\u56e0\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u4ea4\\u6d41\\u4e0d\\u5145\\u5206\\u53ca\\u6c9f\\u901a\\u4e0d\\u7545\\uff1b\\u8f6f\\u4ef6\\u9700\\u6c42\\u7684\\u53d8\\u66f4\\uff1b\\u8f6f\\u4ef6\\u5f00\\u53d1\\u5de5\\u5177\\u7684\\u7f3a\\u9677|B\\uff0e\\u8f6f\\u4ef6\\u7684\\u590d\\u6742\\u6027\\uff1b\\u8f6f\\u4ef6\\u9879\\u76ee\\u7684\\u65f6\\u95f4\\u538b\\u529b|C\\uff0e\\u7a0b\\u5e8f\\u5f00\\u53d1\\u4eba\\u5458\\u7684\\u9519\\u8bef\\uff1b\\u8f6f\\u4ef6\\u9879\\u76ee\\u6587\\u6863\\u7684\\u7f3a\\u4e4f|D\\uff0e\\u4ee5\\u4e0a\\u90fd\\u662f\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 03:51:01', '2020-08-25 03:51:01');
INSERT INTO `admin_operation_log` VALUES (180, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 03:51:02', '2020-08-25 03:51:02');
INSERT INTO `admin_operation_log` VALUES (181, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 04:02:38', '2020-08-25 04:02:38');
INSERT INTO `admin_operation_log` VALUES (182, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-25 05:48:29', '2020-08-25 05:48:29');
INSERT INTO `admin_operation_log` VALUES (183, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:48:33', '2020-08-25 05:48:33');
INSERT INTO `admin_operation_log` VALUES (184, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:48:35', '2020-08-25 05:48:35');
INSERT INTO `admin_operation_log` VALUES (185, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:48:39', '2020-08-25 05:48:39');
INSERT INTO `admin_operation_log` VALUES (186, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 05:48:41', '2020-08-25 05:48:41');
INSERT INTO `admin_operation_log` VALUES (187, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u7f3a\\u9677\\u4ea7\\u751f\\u7684\\u539f\\u56e0\\u662f\\u3010\\u3011\",\"options\":\"A\\uff0e\\u4ea4\\u6d41\\u4e0d\\u5145\\u5206\\u53ca\\u6c9f\\u901a\\u4e0d\\u7545\\uff1b\\u8f6f\\u4ef6\\u9700\\u6c42\\u7684\\u53d8\\u66f4\\uff1b\\u8f6f\\u4ef6\\u5f00\\u53d1\\u5de5\\u5177\\u7684\\u7f3a\\u9677|B\\uff0e\\u8f6f\\u4ef6\\u7684\\u590d\\u6742\\u6027\\uff1b\\u8f6f\\u4ef6\\u9879\\u76ee\\u7684\\u65f6\\u95f4\\u538b\\u529b|C\\uff0e\\u7a0b\\u5e8f\\u5f00\\u53d1\\u4eba\\u5458\\u7684\\u9519\\u8bef\\uff1b\\u8f6f\\u4ef6\\u9879\\u76ee\\u6587\\u6863\\u7684\\u7f3a\\u4e4f|D\\uff0e\\u4ee5\\u4e0a\\u90fd\\u662f\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-25 05:49:35', '2020-08-25 05:49:35');
INSERT INTO `admin_operation_log` VALUES (188, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 05:49:36', '2020-08-25 05:49:36');
INSERT INTO `admin_operation_log` VALUES (189, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:49:42', '2020-08-25 05:49:42');
INSERT INTO `admin_operation_log` VALUES (190, 1, 'admin/questions/72/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:49:55', '2020-08-25 05:49:55');
INSERT INTO `admin_operation_log` VALUES (191, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 05:49:56', '2020-08-25 05:49:56');
INSERT INTO `admin_operation_log` VALUES (192, 1, 'admin/questions/72', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5bfc\\u81f4\\u8f6f\\u4ef6\\u7f3a\\u9677\\u7684\\u6700\\u5927\\u539f\\u56e0\\u662f\\u3010\\u3011\",\"options\":\"A\\uff0e\\u89c4\\u683c\\u8bf4\\u660e\\u4e66|B\\uff0e\\u8bbe\\u8ba1|C\\uff0e\\u7f16\\u7801|D\\uff0e\\u6d4b\\u8bd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 05:50:31', '2020-08-25 05:50:31');
INSERT INTO `admin_operation_log` VALUES (193, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 05:50:31', '2020-08-25 05:50:31');
INSERT INTO `admin_operation_log` VALUES (194, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:50:41', '2020-08-25 05:50:41');
INSERT INTO `admin_operation_log` VALUES (195, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 05:50:41', '2020-08-25 05:50:41');
INSERT INTO `admin_operation_log` VALUES (196, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4fee\\u590d\\u8f6f\\u4ef6\\u7f3a\\u9677\\u8d39\\u7528\\u6700\\u9ad8\\u7684\\u662f\\u3010\\u3011\\u9636\\u6bb5\",\"options\":\"A\\uff0e\\u7f16\\u5236\\u8bf4\\u660e\\u4e66|B\\uff0e\\u8bbe\\u8ba1|C\\uff0e\\u7f16\\u5199\\u4ee3\\u7801|D\\uff0e\\u53d1\\u5e03\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 05:51:07', '2020-08-25 05:51:07');
INSERT INTO `admin_operation_log` VALUES (197, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 05:51:07', '2020-08-25 05:51:07');
INSERT INTO `admin_operation_log` VALUES (198, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:51:27', '2020-08-25 05:51:27');
INSERT INTO `admin_operation_log` VALUES (199, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 05:51:28', '2020-08-25 05:51:28');
INSERT INTO `admin_operation_log` VALUES (200, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u3010\\u3011\\u4e0d\\u5c5e\\u4e8e\\u8f6f\\u4ef6\\u7f3a\\u9677\\u3002\",\"options\":\"A\\uff0e\\u6d4b\\u8bd5\\u4eba\\u5458\\u4e3b\\u89c2\\u8ba4\\u4e3a\\u4e0d\\u5408\\u7406\\u7684\\u5730\\u65b9|B\\uff0e\\u8f6f\\u4ef6\\u672a\\u8fbe\\u5230\\u4ea7\\u54c1\\u8bf4\\u660e\\u4e66\\u6807\\u660e\\u7684\\u529f\\u80fd|C\\uff0e\\u8f6f\\u4ef6\\u51fa\\u73b0\\u4e86\\u4ea7\\u54c1\\u8bf4\\u660e\\u4e66\\u6307\\u660e\\u4e0d\\u4f1a\\u51fa\\u73b0\\u7684\\u9519\\u8bef|D\\uff0e\\u8f6f\\u4ef6\\u529f\\u80fd\\u8d85\\u51fa\\u4ea7\\u54c1\\u8bf4\\u660e\\u4e66\\u6307\\u660e\\u8303\\u56f4\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 05:51:56', '2020-08-25 05:51:56');
INSERT INTO `admin_operation_log` VALUES (201, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 05:51:57', '2020-08-25 05:51:57');
INSERT INTO `admin_operation_log` VALUES (202, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 05:52:10', '2020-08-25 05:52:10');
INSERT INTO `admin_operation_log` VALUES (203, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 05:52:11', '2020-08-25 05:52:11');
INSERT INTO `admin_operation_log` VALUES (204, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u7684\\u76ee\\u7684\\u662f\\u3010\\u3011\",\"options\":\"A\\uff0e\\u907f\\u514d\\u8f6f\\u4ef6\\u5f00\\u53d1\\u4e2d\\u51fa\\u73b0\\u7684\\u9519\\u8bef|B\\uff0e\\u53d1\\u73b0\\u8f6f\\u4ef6\\u5f00\\u53d1\\u4e2d\\u51fa\\u73b0\\u7684\\u9519\\u8bef|C\\uff0e\\u5c3d\\u53ef\\u80fd\\u53d1\\u73b0\\u5e76\\u6392\\u9664\\u8f6f\\u4ef6\\u4e2d\\u6f5c\\u85cf\\u7684\\u9519\\u8bef\\uff0c\\u63d0\\u9ad8\\u8f6f\\u4ef6\\u7684\\u53ef\\u9760\\u6027|D\\uff0e\\u4fee\\u6539\\u8f6f\\u4ef6\\u4e2d\\u51fa\\u73b0\\u7684\\u9519\\u8bef\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 05:52:40', '2020-08-25 05:52:40');
INSERT INTO `admin_operation_log` VALUES (205, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 05:52:41', '2020-08-25 05:52:41');
INSERT INTO `admin_operation_log` VALUES (206, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:06:06', '2020-08-25 06:06:06');
INSERT INTO `admin_operation_log` VALUES (207, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:06:07', '2020-08-25 06:06:07');
INSERT INTO `admin_operation_log` VALUES (208, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u6280\\u672f\\u53ef\\u4ee5\\u5206\\u4e3a\\u9759\\u6001\\u6d4b\\u8bd5\\u548c\\u52a8\\u6001\\u6d4b\\u8bd5\\uff0c\\u4e0b\\u5217\\u8bf4\\u6cd5\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u9759\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u4e0d\\u8fd0\\u884c\\u5b9e\\u9645\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u68c0\\u67e5\\u548c\\u9605\\u8bfb\\u7b49\\u624b\\u6bb5\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8bef\\u3002\\u2002B\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u5b9e\\u9645\\u8fd0\\u884c\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u8fd0\\u884c\\u7684\\u7ed3\\u679c\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8bef\\u3002\\u2002C\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u5305\\u62ec\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u548c\\u767d\\u76d2\\u6d4b\\u8bd5\\u3002\\u2002D\\uff0e\\u767d\\u76d2\\u6d4b\\u8bd5\\u662f\\u9759\\u6001\\u6d4b\\u8bd5\\uff0c\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u52a8\\u6001\\u6d4b\\u8bd5\\u3002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 06:06:41', '2020-08-25 06:06:41');
INSERT INTO `admin_operation_log` VALUES (209, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:06:42', '2020-08-25 06:06:42');
INSERT INTO `admin_operation_log` VALUES (210, 1, 'admin/questions/76/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:06:47', '2020-08-25 06:06:47');
INSERT INTO `admin_operation_log` VALUES (211, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:06:48', '2020-08-25 06:06:48');
INSERT INTO `admin_operation_log` VALUES (212, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:07:02', '2020-08-25 06:07:02');
INSERT INTO `admin_operation_log` VALUES (213, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"4\"}', '2020-08-25 06:07:05', '2020-08-25 06:07:05');
INSERT INTO `admin_operation_log` VALUES (214, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:07:23', '2020-08-25 06:07:23');
INSERT INTO `admin_operation_log` VALUES (215, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:07:24', '2020-08-25 06:07:24');
INSERT INTO `admin_operation_log` VALUES (216, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u6839\\u636e\\u8f6f\\u4ef6\\u7684\\u3010A\\u3011\\u6765\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\",\"options\":\"A\\uff0e\\u529f\\u80fd\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u89c4\\u683c\\u8bf4\\u660e\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u5185\\u90e8\\u903b\\u8f91\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u5185\\u90e8\\u6570\\u636e\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:07:41', '2020-08-25 06:07:41');
INSERT INTO `admin_operation_log` VALUES (217, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:07:42', '2020-08-25 06:07:42');
INSERT INTO `admin_operation_log` VALUES (218, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:07:46', '2020-08-25 06:07:46');
INSERT INTO `admin_operation_log` VALUES (219, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:07:47', '2020-08-25 06:07:47');
INSERT INTO `admin_operation_log` VALUES (220, 1, 'admin/questions/77', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u6839\\u636e\\u8f6f\\u4ef6\\u7684\\u3010\\u3011\\u6765\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\",\"options\":\"A\\uff0e\\u529f\\u80fd\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|B\\uff0e\\u89c4\\u683c\\u8bf4\\u660e\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|C\\uff0e\\u5185\\u90e8\\u903b\\u8f91\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|D\\uff0e\\u5185\\u90e8\\u6570\\u636e\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:07:51', '2020-08-25 06:07:51');
INSERT INTO `admin_operation_log` VALUES (221, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:07:52', '2020-08-25 06:07:52');
INSERT INTO `admin_operation_log` VALUES (222, 1, 'admin/questions/76/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:14:05', '2020-08-25 06:14:05');
INSERT INTO `admin_operation_log` VALUES (223, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:14:06', '2020-08-25 06:14:06');
INSERT INTO `admin_operation_log` VALUES (224, 1, 'admin/questions/76', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u6280\\u672f\\u53ef\\u4ee5\\u5206\\u4e3a\\u9759\\u6001\\u6d4b\\u8bd5\\u548c\\u52a8\\u6001\\u6d4b\\u8bd5\\uff0c\\u4e0b\\u5217\\u8bf4\\u6cd5\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u9759\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u4e0d\\u8fd0\\u884c\\u5b9e\\u9645\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u68c0\\u67e5\\u548c\\u9605\\u8bfb\\u7b49\\u624b\\u6bb5\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8bef|B\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u5b9e\\u9645\\u8fd0\\u884c\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u8fd0\\u884c\\u7684\\u7ed3\\u679c\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8bef|C\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u5305\\u62ec\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u548c\\u767d\\u76d2\\u6d4b\\u8bd5\\u3002\\u2002|D\\uff0e\\u767d\\u76d2\\u6d4b\\u8bd5\\u662f\\u9759\\u6001\\u6d4b\\u8bd5\\uff0c\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u52a8\\u6001\\u6d4b\\u8bd5\\u3002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:14:31', '2020-08-25 06:14:31');
INSERT INTO `admin_operation_log` VALUES (225, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:14:32', '2020-08-25 06:14:32');
INSERT INTO `admin_operation_log` VALUES (226, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:14:36', '2020-08-25 06:14:36');
INSERT INTO `admin_operation_log` VALUES (227, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:14:37', '2020-08-25 06:14:37');
INSERT INTO `admin_operation_log` VALUES (228, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:14:46', '2020-08-25 06:14:46');
INSERT INTO `admin_operation_log` VALUES (229, 1, 'admin/questions/76/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:14:51', '2020-08-25 06:14:51');
INSERT INTO `admin_operation_log` VALUES (230, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:14:51', '2020-08-25 06:14:51');
INSERT INTO `admin_operation_log` VALUES (231, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:14:54', '2020-08-25 06:14:54');
INSERT INTO `admin_operation_log` VALUES (232, 1, 'admin/questions/75/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:14:58', '2020-08-25 06:14:58');
INSERT INTO `admin_operation_log` VALUES (233, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:14:59', '2020-08-25 06:14:59');
INSERT INTO `admin_operation_log` VALUES (234, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:20:51', '2020-08-25 06:20:51');
INSERT INTO `admin_operation_log` VALUES (235, 1, 'admin/questions/76/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:21:33', '2020-08-25 06:21:33');
INSERT INTO `admin_operation_log` VALUES (236, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:21:34', '2020-08-25 06:21:34');
INSERT INTO `admin_operation_log` VALUES (237, 1, 'admin/questions/76', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u6280\\u672f\\u53ef\\u4ee5\\u5206\\u4e3a\\u9759\\u6001\\u6d4b\\u8bd5\\u548c\\u52a8\\u6001\\u6d4b\\u8bd5\\uff0c\\u4e0b\\u5217\\u8bf4\\u6cd5\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u9759\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u4e0d\\u8fd0\\u884c\\u5b9e\\u9645\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u68c0\\u67e5\\u548c\\u9605\\u8bfb\\u7b49\\u624b\\u6bb5\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8befB\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u5b9e\\u9645\\u8fd0\\u884c\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u8fd0\\u884c\\u7684\\u7ed3\\u679c\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8befC\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u5305\\u62ec\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u548c\\u767d\\u76d2\\u6d4b\\u8bd5\\u3002\\u2002D\\uff0e\\u767d\\u76d2\\u6d4b\\u8bd5\\u662f\\u9759\\u6001\\u6d4b\\u8bd5\\uff0c\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u52a8\\u6001\\u6d4b\\u8bd5\\u3002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:21:44', '2020-08-25 06:21:44');
INSERT INTO `admin_operation_log` VALUES (238, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:21:44', '2020-08-25 06:21:44');
INSERT INTO `admin_operation_log` VALUES (239, 1, 'admin/questions/76/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:22:15', '2020-08-25 06:22:15');
INSERT INTO `admin_operation_log` VALUES (240, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:22:16', '2020-08-25 06:22:16');
INSERT INTO `admin_operation_log` VALUES (241, 1, 'admin/questions/76', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u6280\\u672f\\u53ef\\u4ee5\\u5206\\u4e3a\\u9759\\u6001\\u6d4b\\u8bd5\\u548c\\u52a8\\u6001\\u6d4b\\u8bd5\\uff0c\\u4e0b\\u5217\\u8bf4\\u6cd5\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u9759\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u4e0d\\u8fd0\\u884c\\u5b9e\\u9645\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u68c0\\u67e5\\u548c\\u9605\\u8bfb\\u7b49\\u624b\\u6bb5\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8befB\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u662f\\u6307\\u5b9e\\u9645\\u8fd0\\u884c\\u7a0b\\u5e8f\\uff0c\\u901a\\u8fc7\\u8fd0\\u884c\\u7684\\u7ed3\\u679c\\u6765\\u53d1\\u73b0\\u7a0b\\u5e8f\\u4e2d\\u7684\\u9519\\u8befC\\uff0e\\u52a8\\u6001\\u6d4b\\u8bd5\\u5305\\u62ec\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u548c\\u767d\\u76d2\\u6d4b\\u8bd5\\u3002\\u2002D\\uff0e\\u767d\\u76d2\\u6d4b\\u8bd5\\u662f\\u9759\\u6001\\u6d4b\\u8bd5\\uff0c\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u52a8\\u6001\\u6d4b\\u8bd5\\u3002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:22:18', '2020-08-25 06:22:18');
INSERT INTO `admin_operation_log` VALUES (242, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:22:18', '2020-08-25 06:22:18');
INSERT INTO `admin_operation_log` VALUES (243, 1, 'admin/questions/76/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:24:35', '2020-08-25 06:24:35');
INSERT INTO `admin_operation_log` VALUES (244, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:24:36', '2020-08-25 06:24:36');
INSERT INTO `admin_operation_log` VALUES (245, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:25:13', '2020-08-25 06:25:13');
INSERT INTO `admin_operation_log` VALUES (246, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"4\"}', '2020-08-25 06:25:15', '2020-08-25 06:25:15');
INSERT INTO `admin_operation_log` VALUES (247, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:25:19', '2020-08-25 06:25:19');
INSERT INTO `admin_operation_log` VALUES (248, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:25:20', '2020-08-25 06:25:20');
INSERT INTO `admin_operation_log` VALUES (249, 1, 'admin/questions/77', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u6839\\u636e\\u8f6f\\u4ef6\\u7684\\u3010\\u3011\\u6765\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\",\"options\":\"A\\uff0e\\u529f\\u80fdB\\uff0e\\u89c4\\u683c\\u8bf4\\u660eC\\uff0e\\u5185\\u90e8\\u903b\\u8f91D\\uff0e\\u5185\\u90e8\\u6570\\u636e\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:26:07', '2020-08-25 06:26:07');
INSERT INTO `admin_operation_log` VALUES (250, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:26:08', '2020-08-25 06:26:08');
INSERT INTO `admin_operation_log` VALUES (251, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:41:07', '2020-08-25 06:41:07');
INSERT INTO `admin_operation_log` VALUES (252, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:41:08', '2020-08-25 06:41:08');
INSERT INTO `admin_operation_log` VALUES (253, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:41:10', '2020-08-25 06:41:10');
INSERT INTO `admin_operation_log` VALUES (254, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:41:29', '2020-08-25 06:41:29');
INSERT INTO `admin_operation_log` VALUES (255, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:41:33', '2020-08-25 06:41:33');
INSERT INTO `admin_operation_log` VALUES (256, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:41:33', '2020-08-25 06:41:33');
INSERT INTO `admin_operation_log` VALUES (257, 1, 'admin/questions/77', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u662f\\u6839\\u636e\\u8f6f\\u4ef6\\u7684\\u3010\\u3011\\u6765\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\",\"options\":\"A\\uff0e\\u529f\\u80fd|B\\uff0e\\u89c4\\u683c\\u8bf4\\u660e|C\\uff0e\\u5185\\u90e8\\u903b\\u8f91|D\\uff0e\\u5185\\u90e8\\u6570\\u636e\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=4\"}', '2020-08-25 06:41:36', '2020-08-25 06:41:36');
INSERT INTO `admin_operation_log` VALUES (258, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 06:41:36', '2020-08-25 06:41:36');
INSERT INTO `admin_operation_log` VALUES (259, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 06:41:40', '2020-08-25 06:41:40');
INSERT INTO `admin_operation_log` VALUES (260, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:41:41', '2020-08-25 06:41:41');
INSERT INTO `admin_operation_log` VALUES (261, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '[]', '2020-08-25 06:43:18', '2020-08-25 06:43:18');
INSERT INTO `admin_operation_log` VALUES (262, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:43:19', '2020-08-25 06:43:19');
INSERT INTO `admin_operation_log` VALUES (263, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '[]', '2020-08-25 06:45:16', '2020-08-25 06:45:16');
INSERT INTO `admin_operation_log` VALUES (264, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 06:45:17', '2020-08-25 06:45:17');
INSERT INTO `admin_operation_log` VALUES (265, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '[]', '2020-08-25 07:00:01', '2020-08-25 07:00:01');
INSERT INTO `admin_operation_log` VALUES (266, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:00:02', '2020-08-25 07:00:02');
INSERT INTO `admin_operation_log` VALUES (267, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '[]', '2020-08-25 07:01:24', '2020-08-25 07:01:24');
INSERT INTO `admin_operation_log` VALUES (268, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:01:25', '2020-08-25 07:01:25');
INSERT INTO `admin_operation_log` VALUES (269, 1, 'admin/questions/77/edit', 'GET', '192.168.1.100', '[]', '2020-08-25 07:18:09', '2020-08-25 07:18:09');
INSERT INTO `admin_operation_log` VALUES (270, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:18:09', '2020-08-25 07:18:09');
INSERT INTO `admin_operation_log` VALUES (271, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:18:12', '2020-08-25 07:18:12');
INSERT INTO `admin_operation_log` VALUES (272, 1, 'admin/questions/1/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:18:16', '2020-08-25 07:18:16');
INSERT INTO `admin_operation_log` VALUES (273, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:18:17', '2020-08-25 07:18:17');
INSERT INTO `admin_operation_log` VALUES (274, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:18:50', '2020-08-25 07:18:50');
INSERT INTO `admin_operation_log` VALUES (275, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:18:52', '2020-08-25 07:18:52');
INSERT INTO `admin_operation_log` VALUES (276, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:18:53', '2020-08-25 07:18:53');
INSERT INTO `admin_operation_log` VALUES (277, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u7528\\u9ed1\\u76d2\\u6280\\u672f\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\\u7684\\u65b9\\u6cd5\\u4e4b\\u4e00\\u4e3a\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u56e0\\u679c\\u56fe\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u903b\\u8f91\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u5faa\\u73af\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u57fa\\u672c\\u8def\\u5f84\\u6d4b\\u8bd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-25 07:19:11', '2020-08-25 07:19:11');
INSERT INTO `admin_operation_log` VALUES (278, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 07:19:11', '2020-08-25 07:19:11');
INSERT INTO `admin_operation_log` VALUES (279, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:19:23', '2020-08-25 07:19:23');
INSERT INTO `admin_operation_log` VALUES (280, 1, 'admin/questions/78/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:19:27', '2020-08-25 07:19:27');
INSERT INTO `admin_operation_log` VALUES (281, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:19:27', '2020-08-25 07:19:27');
INSERT INTO `admin_operation_log` VALUES (282, 1, 'admin/questions/78', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u7528\\u9ed1\\u76d2\\u6280\\u672f\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\\u7684\\u65b9\\u6cd5\\u4e4b\\u4e00\\u4e3a\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u56e0\\u679c\\u56fe\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u903b\\u8f91\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u5faa\\u73af\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u57fa\\u672c\\u8def\\u5f84\\u6d4b\\u8bd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 07:19:55', '2020-08-25 07:19:55');
INSERT INTO `admin_operation_log` VALUES (283, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"4\"}', '2020-08-25 07:19:55', '2020-08-25 07:19:55');
INSERT INTO `admin_operation_log` VALUES (284, 1, 'admin/questions/78/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:20:00', '2020-08-25 07:20:00');
INSERT INTO `admin_operation_log` VALUES (285, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:20:01', '2020-08-25 07:20:01');
INSERT INTO `admin_operation_log` VALUES (286, 1, 'admin/questions/78', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u7528\\u9ed1\\u76d2\\u6280\\u672f\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\\u7684\\u65b9\\u6cd5\\u4e4b\\u4e00\\u4e3a\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u56e0\\u679c\\u56fe\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|B\\uff0e\\u903b\\u8f91\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|C\\uff0e\\u5faa\\u73af\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|D\\uff0e\\u57fa\\u672c\\u8def\\u5f84\\u6d4b\\u8bd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=4\"}', '2020-08-25 07:29:06', '2020-08-25 07:29:06');
INSERT INTO `admin_operation_log` VALUES (287, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:29:06', '2020-08-25 07:29:06');
INSERT INTO `admin_operation_log` VALUES (288, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:29:07', '2020-08-25 07:29:07');
INSERT INTO `admin_operation_log` VALUES (289, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u575a\\u6301\\u5728\\u8f6f\\u4ef6\\u7684\\u5404\\u4e2a\\u9636\\u6bb5\\u5b9e\\u65bd\\u4e0b\\u5217\\u54ea\\u79cd\\u8d28\\u91cf\\u4fdd\\u969c\\u63aa\\u65bd\\uff0c\\u624d\\u80fd\\u5728\\u5f00\\u53d1\\u8fc7\\u7a0b\\u4e2d\\u5c3d\\u65e9\\u53d1\\u73b0\\u548c\\u9884\\u9632\\u9519\\u8bef\\uff0c\\u628a\\u51fa\\u73b0\\u7684\\u9519\\u8bef\\u514b\\u670d\\u5728\\u65e9\\u671f\\u3010\\u3011\\u3002\",\"options\":\"\\u2002 A\\uff0e\\u6280\\u672f\\u8bc4\\u5ba1\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u7a0b\\u5e8f\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u6539\\u6b63\\u7a0b\\u5e8f\\u9519\\u8bef\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u7ba1\\u7406\\u8bc4\\u5ba1\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\\/78\\/edit\"}', '2020-08-25 07:29:26', '2020-08-25 07:29:26');
INSERT INTO `admin_operation_log` VALUES (290, 1, 'admin/questions/78/edit', 'GET', '192.168.1.100', '[]', '2020-08-25 07:29:27', '2020-08-25 07:29:27');
INSERT INTO `admin_operation_log` VALUES (291, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:29:28', '2020-08-25 07:29:28');
INSERT INTO `admin_operation_log` VALUES (292, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:29:31', '2020-08-25 07:29:31');
INSERT INTO `admin_operation_log` VALUES (293, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"4\"}', '2020-08-25 07:29:34', '2020-08-25 07:29:34');
INSERT INTO `admin_operation_log` VALUES (294, 1, 'admin/questions/79/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:29:38', '2020-08-25 07:29:38');
INSERT INTO `admin_operation_log` VALUES (295, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:29:39', '2020-08-25 07:29:39');
INSERT INTO `admin_operation_log` VALUES (296, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:29:44', '2020-08-25 07:29:44');
INSERT INTO `admin_operation_log` VALUES (297, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:29:46', '2020-08-25 07:29:46');
INSERT INTO `admin_operation_log` VALUES (298, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:29:47', '2020-08-25 07:29:47');
INSERT INTO `admin_operation_log` VALUES (299, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e3a\\u4e86\\u63d0\\u9ad8\\u6d4b\\u8bd5\\u7684\\u6548\\u7387\\uff0c\\u6b63\\u786e\\u7684\\u505a\\u6cd5\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u9009\\u62e9\\u53d1\\u73b0\\u9519\\u8bef\\u53ef\\u80fd\\u6027\\u5927\\u7684\\u6570\\u636e\\u4f5c\\u4e3a\\u6d4b\\u8bd5\\u7528\\u4f8b\\u2002B\\uff0e\\u5728\\u5b8c\\u6210\\u7a0b\\u5e8f\\u7684\\u7f16\\u7801\\u4e4b\\u540e\\u518d\\u5236\\u5b9a\\u8f6f\\u4ef6\\u7684\\u6d4b\\u8bd5\\u8ba1\\u5212\\u2002C\\uff0e\\u968f\\u673a\\u9009\\u53d6\\u6d4b\\u8bd5\\u7528\\u4f8b\\u2002D\\uff0e\\u4f7f\\u7528\\u6d4b\\u8bd5\\u7528\\u4f8b\\u6d4b\\u8bd5\\u662f\\u4e3a\\u4e86\\u68c0\\u67e5\\u7a0b\\u5e8f\\u662f\\u5426\\u505a\\u4e86\\u5e94\\u8be5\\u505a\\u7684\\u4e8b\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-25 07:30:14', '2020-08-25 07:30:14');
INSERT INTO `admin_operation_log` VALUES (300, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:30:14', '2020-08-25 07:30:14');
INSERT INTO `admin_operation_log` VALUES (301, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:30:15', '2020-08-25 07:30:15');
INSERT INTO `admin_operation_log` VALUES (302, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5bf9\\u7a0b\\u5e8f\\u7684\\u6d4b\\u8bd5\\u6700\\u597d\\u7531\\u3010\\u3011\\u6765\\u505a\\u3002\",\"options\":\"\\u2002 A\\uff0e\\u7a0b\\u5e8f\\u5458\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u7b2c\\u4e09\\u65b9\\u6d4b\\u8bd5\\u673a\\u6784\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u7a0b\\u5e8f\\u5f00\\u53d1\\u7ec4\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u7528\\u6237\\u2002\\u2002\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:30:34', '2020-08-25 07:30:34');
INSERT INTO `admin_operation_log` VALUES (303, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:30:34', '2020-08-25 07:30:34');
INSERT INTO `admin_operation_log` VALUES (304, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:30:35', '2020-08-25 07:30:35');
INSERT INTO `admin_operation_log` VALUES (305, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5728\\u8fb9\\u754c\\u503c\\u5206\\u6790\\u4e2d\\uff0c\\u4e0b\\u5217\\u6570\\u636e\\u901a\\u5e38\\u4e0d\\u7528\\u6765\\u505a\\u6570\\u636e\\u6d4b\\u8bd5\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u6b63\\u597d\\u7b49\\u4e8e\\u8fb9\\u754c\\u7684\\u503c\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u7b49\\u4ef7\\u7c7b\\u4e2d\\u7684\\u7b49\\u4ef7\\u503c\\u2002C\\uff0e\\u521a\\u521a\\u5927\\u4e8e\\u8fb9\\u754c\\u7684\\u503c\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u521a\\u521a\\u5c0f\\u4e8e\\u8fb9\\u754c\\u7684\\u503c\\u2002\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:30:51', '2020-08-25 07:30:51');
INSERT INTO `admin_operation_log` VALUES (306, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:30:52', '2020-08-25 07:30:52');
INSERT INTO `admin_operation_log` VALUES (307, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:30:53', '2020-08-25 07:30:53');
INSERT INTO `admin_operation_log` VALUES (308, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5355\\u5143\\u6d4b\\u8bd5\\u4e2d\\u8bbe\\u8ba1\\u6d4b\\u8bd5\\u7528\\u4f8b\\u7684\\u4f9d\\u636e\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u6982\\u8981\\u8bbe\\u8ba1\\u89c4\\u683c\\u8bf4\\u660e\\u4e66\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u7528\\u6237\\u9700\\u6c42\\u89c4\\u683c\\u8bf4\\u660e\\u4e66\\u2002C\\uff0e\\u9879\\u76ee\\u8ba1\\u5212\\u8bf4\\u660e\\u4e66\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u8be6\\u7ec6\\u8bbe\\u8ba1\\u89c4\\u683c\\u8bf4\\u660e\\u4e66\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:31:09', '2020-08-25 07:31:09');
INSERT INTO `admin_operation_log` VALUES (309, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:31:09', '2020-08-25 07:31:09');
INSERT INTO `admin_operation_log` VALUES (310, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:31:10', '2020-08-25 07:31:10');
INSERT INTO `admin_operation_log` VALUES (311, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5982\\u679c\\u4e00\\u4e2a\\u5224\\u5b9a\\u4e2d\\u7684\\u590d\\u5408\\u6761\\u4ef6\\u8868\\u8fbe\\u5f0f\\u4e3a\\uff08A\\u2002>\\u20021\\uff09or\\uff08B\\u2002<=\\u20023\\uff09\\uff0c\\u5219\\u4e3a\\u4e86\\u8fbe\\u5230100%\\u7684\\u6761\\u4ef6\\u8986\\u76d6\\u7387\\uff0c\\u81f3\\u5c11\\u9700\\u8981\\u8bbe\\u8ba1\\u591a\\u5c11\\u4e2a\\u6d4b\\u8bd5\\u7528\\u4f8b\\u3010\\u3011\\u3002\",\"options\":\"\\u2002 A\\uff0e1\\u2002B\\uff0e2\\u2002C\\uff0e3\\u2002D\\uff0e4\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:32:02', '2020-08-25 07:32:02');
INSERT INTO `admin_operation_log` VALUES (312, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:32:02', '2020-08-25 07:32:02');
INSERT INTO `admin_operation_log` VALUES (313, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:32:03', '2020-08-25 07:32:03');
INSERT INTO `admin_operation_log` VALUES (314, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5728\\u67d0\\u5927\\u5b66\\u5b66\\u7c4d\\u7ba1\\u7406\\u4fe1\\u606f\\u7cfb\\u7edf\\u4e2d\\uff0c\\u5047\\u8bbe\\u5b66\\u751f\\u5e74\\u9f84\\u7684\\u8f93\\u5165\\u8303\\u56f4\\u4e3a16\\u201440\\uff0c\\u5219\\u6839\\u636e\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u4e2d\\u7684\\u7b49\\u4ef7\\u7c7b\\u5212\\u5206\\u6280\\u672f\\uff0c\\u4e0b\\u9762\\u5212\\u5206\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u53ef\\u5212\\u5206\\u4e3a2\\u4e2a\\u6709\\u6548\\u7b49\\u4ef7\\u7c7b\\uff0c2\\u4e2a\\u65e0\\u6548\\u7b49\\u4ef7\\u7c7b\\u2002B\\uff0e\\u53ef\\u5212\\u5206\\u4e3a1\\u4e2a\\u6709\\u6548\\u7b49\\u4ef7\\u7c7b\\uff0c2\\u4e2a\\u65e0\\u6548\\u7b49\\u4ef7\\u7c7b\\u2002C\\uff0e\\u53ef\\u5212\\u5206\\u4e3a2\\u4e2a\\u6709\\u6548\\u7b49\\u4ef7\\u7c7b\\uff0c1\\u4e2a\\u65e0\\u6548\\u7b49\\u4ef7\\u7c7b\\u2002D\\uff0e\\u53ef\\u5212\\u5206\\u4e3a1\\u4e2a\\u6709\\u6548\\u7b49\\u4ef7\\u7c7b\\uff0c1\\u4e2a\\u65e0\\u6548\\u7b49\\u4ef7\\u7c7b\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:32:35', '2020-08-25 07:32:35');
INSERT INTO `admin_operation_log` VALUES (315, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:32:36', '2020-08-25 07:32:36');
INSERT INTO `admin_operation_log` VALUES (316, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:32:37', '2020-08-25 07:32:37');
INSERT INTO `admin_operation_log` VALUES (317, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u9762\\u6709\\u5173\\u6d4b\\u8bd5\\u539f\\u5219\\u7684\\u8bf4\\u6cd5\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\",\"options\":\"A\\uff0e\\u6d4b\\u8bd5\\u7528\\u4f8b\\u5e94\\u7531\\u6d4b\\u8bd5\\u7684\\u8f93\\u5165\\u6570\\u636e\\u548c\\u9884\\u671f\\u7684\\u8f93\\u51fa\\u7ed3\\u679c\\u7ec4\\u6210\\u2002B\\uff0e\\u6d4b\\u8bd5\\u7528\\u4f8b\\u53ea\\u9700\\u9009\\u53d6\\u5408\\u7406\\u7684\\u8f93\\u5165\\u6570\\u636e\\u2002C\\uff0e\\u7a0b\\u5e8f\\u6700\\u597d\\u7531\\u7f16\\u5199\\u8be5\\u7a0b\\u5e8f\\u7684\\u7a0b\\u5e8f\\u5458\\u81ea\\u5df1\\u6765\\u6d4b\\u8bd5\\u2002 D\\uff0e\\u4f7f\\u7528\\u6d4b\\u8bd5\\u7528\\u4f8b\\u8fdb\\u884c\\u6d4b\\u8bd5\\u662f\\u4e3a\\u4e86\\u68c0\\u67e5\\u7a0b\\u5e8f\\u662f\\u5426\\u505a\\u4e86\\u5b83\\u8be5\\u505a\\u7684\\u4e8b\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\"}', '2020-08-25 07:33:03', '2020-08-25 07:33:03');
INSERT INTO `admin_operation_log` VALUES (318, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 07:33:03', '2020-08-25 07:33:03');
INSERT INTO `admin_operation_log` VALUES (319, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:35:51', '2020-08-25 07:35:51');
INSERT INTO `admin_operation_log` VALUES (320, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:35:51', '2020-08-25 07:35:51');
INSERT INTO `admin_operation_log` VALUES (321, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5173\\u4e8e\\u6d4b\\u8bd5\\u65b9\\u6cd5\\u7684\\u53d9\\u8ff0\\u4e2d\\u4e0d\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\",\"options\":\"\\u2002 A\\uff0e\\u4ece\\u67d0\\u79cd\\u89d2\\u5ea6\\u4e0a\\u8bb2\\uff0c\\u767d\\u76d2\\u6d4b\\u8bd5\\u4e0e\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u90fd\\u5c5e\\u4e8e\\u52a8\\u6001\\u6d4b\\u8bd5\\u2002B\\uff0e\\u529f\\u80fd\\u6d4b\\u8bd5\\u5c5e\\u4e8e\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u2002C\\uff0e\\u5bf9\\u529f\\u80fd\\u7684\\u6d4b\\u8bd5\\u901a\\u5e38\\u662f\\u8981\\u8003\\u8651\\u7a0b\\u5e8f\\u7684\\u5185\\u90e8\\u7ed3\\u6784\\u2002D\\uff0e\\u7ed3\\u6784\\u6d4b\\u8bd5\\u5c5e\\u4e8e\\u767d\\u76d2\\u6d4b\\u8bd5\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-25 07:36:06', '2020-08-25 07:36:06');
INSERT INTO `admin_operation_log` VALUES (322, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:36:07', '2020-08-25 07:36:07');
INSERT INTO `admin_operation_log` VALUES (323, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:36:08', '2020-08-25 07:36:08');
INSERT INTO `admin_operation_log` VALUES (324, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:36:11', '2020-08-25 07:36:11');
INSERT INTO `admin_operation_log` VALUES (325, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"5\"}', '2020-08-25 07:36:18', '2020-08-25 07:36:18');
INSERT INTO `admin_operation_log` VALUES (326, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:41:02', '2020-08-25 07:41:02');
INSERT INTO `admin_operation_log` VALUES (327, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:41:02', '2020-08-25 07:41:02');
INSERT INTO `admin_operation_log` VALUES (328, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u65b9\\u6cd5\\u4e2d\\uff0c\\u4e0d\\u5c5e\\u4e8e\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u7684\\u662f\\u3010A\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u57fa\\u672c\\u8def\\u5f84\\u6d4b\\u8bd5\\u6cd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u7b49\\u4ef7\\u7c7b\\u6d4b\\u8bd5\\u6cd5\\u2002C\\uff0e\\u8fb9\\u754c\\u503c\\u5206\\u6790\\u6cd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u57fa\\u4e8e\\u573a\\u666f\\u7684\\u6d4b\\u8bd5\\u65b9\\u6cd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=5\"}', '2020-08-25 07:42:01', '2020-08-25 07:42:01');
INSERT INTO `admin_operation_log` VALUES (329, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:42:02', '2020-08-25 07:42:02');
INSERT INTO `admin_operation_log` VALUES (330, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:42:03', '2020-08-25 07:42:03');
INSERT INTO `admin_operation_log` VALUES (331, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0d\\u5c5e\\u4e8e\\u767d\\u76d2\\u6d4b\\u8bd5\\u7684\\u6280\\u672f\\u662f\\u2002\\u3010\\u3011\\u3002\",\"options\":\"A\\uff0e\\u8bed\\u53e5\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u5224\\u5b9a\\u8986\\u76d6\\u2002\\u2002C\\uff0e\\u8fb9\\u754c\\u503c\\u5206\\u6790\\u2002D\\uff0e\\u57fa\\u672c\\u8def\\u5f84\\u6d4b\\u8bd5\\u2002\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:42:21', '2020-08-25 07:42:21');
INSERT INTO `admin_operation_log` VALUES (332, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:42:22', '2020-08-25 07:42:22');
INSERT INTO `admin_operation_log` VALUES (333, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:42:23', '2020-08-25 07:42:23');
INSERT INTO `admin_operation_log` VALUES (334, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u6d4b\\u8bd5\\u7a0b\\u5e8f\\u65f6\\uff0c\\uff08\\u7a77\\u4e3e\\u3001\\u7a77\\u5c3d\\uff09\\u4e0d\\u53ef\\u80fd\\u904d\\u5386\\u6240\\u6709\\u53ef\\u80fd\\u7684\\u8f93\\u5165\\u6570\\u636e\\uff0c\\u800c\\u53ea\\u80fd\\u662f\\u9009\\u62e9\\u4e00\\u4e2a\\u5b50\\u96c6\\u8fdb\\u884c\\u6d4b\\u8bd5\\uff0c\\u90a3\\u4e48\\u6700\\u597d\\u7684\\u9009\\u62e9\\u65b9\\u6cd5\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u968f\\u673a\\u9009\\u62e9\\u2002B\\uff0e\\u5212\\u5206\\u7b49\\u4ef7\\u7c7b\\u2002 C\\uff0e\\u6839\\u636e\\u63a5\\u53e3\\u8fdb\\u884c\\u9009\\u62e9\\u2002D\\uff0e\\u6839\\u636e\\u6570\\u636e\\u5927\\u5c0f\\u8fdb\\u884c\\u9009\\u62e9\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:42:42', '2020-08-25 07:42:42');
INSERT INTO `admin_operation_log` VALUES (335, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:42:42', '2020-08-25 07:42:42');
INSERT INTO `admin_operation_log` VALUES (336, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:42:43', '2020-08-25 07:42:43');
INSERT INTO `admin_operation_log` VALUES (337, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:43:07', '2020-08-25 07:43:07');
INSERT INTO `admin_operation_log` VALUES (338, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"5\"}', '2020-08-25 07:43:09', '2020-08-25 07:43:09');
INSERT INTO `admin_operation_log` VALUES (339, 1, 'admin/questions/88/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:43:14', '2020-08-25 07:43:14');
INSERT INTO `admin_operation_log` VALUES (340, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:43:15', '2020-08-25 07:43:15');
INSERT INTO `admin_operation_log` VALUES (341, 1, 'admin/questions/88', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u65b9\\u6cd5\\u4e2d\\uff0c\\u4e0d\\u5c5e\\u4e8e\\u9ed1\\u76d2\\u6d4b\\u8bd5\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u57fa\\u672c\\u8def\\u5f84\\u6d4b\\u8bd5\\u6cd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|B\\uff0e\\u7b49\\u4ef7\\u7c7b\\u6d4b\\u8bd5\\u6cd5\\u2002|C\\uff0e\\u8fb9\\u754c\\u503c\\u5206\\u6790\\u6cd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002|D\\uff0e\\u57fa\\u4e8e\\u573a\\u666f\\u7684\\u6d4b\\u8bd5\\u65b9\\u6cd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=5\"}', '2020-08-25 07:43:18', '2020-08-25 07:43:18');
INSERT INTO `admin_operation_log` VALUES (342, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\"}', '2020-08-25 07:43:19', '2020-08-25 07:43:19');
INSERT INTO `admin_operation_log` VALUES (343, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:44:20', '2020-08-25 07:44:20');
INSERT INTO `admin_operation_log` VALUES (344, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:44:20', '2020-08-25 07:44:20');
INSERT INTO `admin_operation_log` VALUES (345, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u53ef\\u4ee5\\u4f5c\\u4e3a\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u5bf9\\u8c61\\u7684\\u662f\\u3010\\u3011\\u3002\",\"options\":\"A\\uff0e\\u9700\\u6c42\\u89c4\\u683c\\u8bf4\\u660e\\u4e66B\\uff0e\\u8f6f\\u4ef6\\u8bbe\\u8ba1\\u89c4\\u683c\\u8bf4\\u660e\\u2002C\\uff0e\\u6e90\\u7a0b\\u5e8fD\\uff0e\\u4ee5\\u4e0a\\u5168\\u90e8\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=5\"}', '2020-08-25 07:44:42', '2020-08-25 07:44:42');
INSERT INTO `admin_operation_log` VALUES (346, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:44:43', '2020-08-25 07:44:43');
INSERT INTO `admin_operation_log` VALUES (347, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:44:44', '2020-08-25 07:44:44');
INSERT INTO `admin_operation_log` VALUES (348, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5728\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u9636\\u6bb5\\uff0c\\u6d4b\\u8bd5\\u6b65\\u9aa4\\u6309\\u6b21\\u5e8f\\u53ef\\u4ee5\\u5212\\u5206\\u4e3a\\u4ee5\\u4e0b\\u51e0\\u6b65\\uff1a\\u3010\\u3011\",\"options\":\"\\u2002A\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u3001\\u96c6\\u6210\\u6d4b\\u8bd5\\u3001\\u7cfb\\u7edf\\u6d4b\\u8bd5\\u3001\\u9a8c\\u6536\\u6d4b\\u8bd5\\u2002B\\uff0e\\u9a8c\\u6536\\u6d4b\\u8bd5\\u3001\\u5355\\u5143\\u6d4b\\u8bd5\\u3001\\u7cfb\\u7edf\\u6d4b\\u8bd5\\u3001\\u96c6\\u6210\\u6d4b\\u8bd5\\u2002C\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u3001\\u96c6\\u6210\\u6d4b\\u8bd5\\u3001\\u9a8c\\u6536\\u6d4b\\u8bd5\\u3001\\u7cfb\\u7edf\\u6d4b\\u8bd5\\u2002D\\uff0e\\u7cfb\\u7edf\\u6d4b\\u8bd5\\u3001\\u5355\\u5143\\u6d4b\\u8bd5\\u3001\\u96c6\\u6210\\u6d4b\\u8bd5\\u3001\\u9a8c\\u6536\\u6d4b\\u8bd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:45:03', '2020-08-25 07:45:03');
INSERT INTO `admin_operation_log` VALUES (349, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:45:04', '2020-08-25 07:45:04');
INSERT INTO `admin_operation_log` VALUES (350, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:45:05', '2020-08-25 07:45:05');
INSERT INTO `admin_operation_log` VALUES (351, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u8fc7\\u7a0b\\u4e2d\\u7684\\u96c6\\u6210\\u6d4b\\u8bd5\\u4e3b\\u8981\\u662f\\u4e3a\\u4e86\\u53d1\\u73b0\\u3010\\u3011\\u9636\\u6bb5\\u7684\\u9519\\u8bef\\u3002\",\"options\":\"A.\\u9700\\u6c42\\u5206\\u6790\\u2002B.\\u6982\\u8981\\u8bbe\\u8ba1\\u2002C.\\u8be6\\u7ec6\\u8bbe\\u8ba1\\u2002D.\\u7f16\\u7801\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\"}', '2020-08-25 07:46:00', '2020-08-25 07:46:00');
INSERT INTO `admin_operation_log` VALUES (352, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 07:46:00', '2020-08-25 07:46:00');
INSERT INTO `admin_operation_log` VALUES (353, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:46:04', '2020-08-25 07:46:04');
INSERT INTO `admin_operation_log` VALUES (354, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:46:27', '2020-08-25 07:46:27');
INSERT INTO `admin_operation_log` VALUES (355, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:46:28', '2020-08-25 07:46:28');
INSERT INTO `admin_operation_log` VALUES (356, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u6307\\u5bfc\\u9009\\u62e9\\u548c\\u4f7f\\u7528\\u6d4b\\u8bd5\\u8986\\u76d6\\u7387\\u7684\\u539f\\u5219\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u3002\",\"options\":\"\\u2002A\\uff0e\\u8986\\u76d6\\u7387\\u4e0d\\u662f\\u76ee\\u7684\\uff0c\\u4ec5\\u662f\\u4e00\\u79cd\\u624b\\u6bb5\\u2002B\\uff0e\\u4e0d\\u8981\\u8ffd\\u6c42\\u7edd\\u5bf9100%\\u7684\\u8986\\u76d6\\u7387\\u2002 C\\uff0e\\u4e0d\\u53ef\\u80fd\\u9488\\u5bf9\\u6240\\u6709\\u7684\\u8986\\u76d6\\u7387\\u6307\\u6807\\u6765\\u9009\\u62e9\\u6d4b\\u8bd5\\u7528\\u4f8b\\u2002D\\uff0e\\u53ea\\u6839\\u636e\\u6d4b\\u8bd5\\u8986\\u76d6\\u7387\\u6307\\u6807\\u6765\\u6307\\u5bfc\\u6d4b\\u8bd5\\u7528\\u4f8b\\u7684\\u8bbe\\u8ba1\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=5\"}', '2020-08-25 07:46:41', '2020-08-25 07:46:41');
INSERT INTO `admin_operation_log` VALUES (357, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:46:42', '2020-08-25 07:46:42');
INSERT INTO `admin_operation_log` VALUES (358, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:46:43', '2020-08-25 07:46:43');
INSERT INTO `admin_operation_log` VALUES (359, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u6d4b\\u8bd5\\u6587\\u6863\\u79cd\\u7c7b\\u5305\\u62ec\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u9700\\u6c42\\u7c7b\\u6587\\u6863\\u3001\\u8ba1\\u5212\\u7c7b\\u6587\\u6863\\u2002B\\uff0e\\u8bbe\\u8ba1\\u7c7b\\u6587\\u6863\\u3001\\u6267\\u884c\\u7c7b\\u6587\\u6863\\u2002C.\\u2002\\u7f3a\\u9677\\u8bb0\\u5f55\\u7c7b\\u3001\\u9636\\u6bb5\\u6c47\\u603b\\u7c7b\\u2002\\u6d4b\\u8bd5\\u603b\\u7ed3\\u7c7b\\u2002D\\uff0e\\u4ee5\\u4e0a\\u90fd\\u6709\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\"}', '2020-08-25 07:46:59', '2020-08-25 07:46:59');
INSERT INTO `admin_operation_log` VALUES (360, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 07:47:00', '2020-08-25 07:47:00');
INSERT INTO `admin_operation_log` VALUES (361, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:47:41', '2020-08-25 07:47:41');
INSERT INTO `admin_operation_log` VALUES (362, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:47:55', '2020-08-25 07:47:55');
INSERT INTO `admin_operation_log` VALUES (363, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:47:56', '2020-08-25 07:47:56');
INSERT INTO `admin_operation_log` VALUES (364, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4ee5\\u4e0b\\u5173\\u4e8e\\u8f6f\\u4ef6\\u56de\\u5f52\\u6d4b\\u8bd5\\u7684\\u8bf4\\u6cd5\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u8f6f\\u4ef6\\u53d8\\u66f4\\u540e\\uff0c\\u5e94\\u5bf9\\u8f6f\\u4ef6\\u53d8\\u66f4\\u90e8\\u5206\\u7684\\u6b63\\u786e\\u6027\\u548c\\u5bf9\\u53d8\\u66f4\\u9700\\u6c42\\u7684\\u7b26\\u5408\\u6027\\u8fdb\\u884c\\u6d4b\\u8bd5\\u2002B\\uff0e\\u8f6f\\u4ef6\\u53d8\\u66f4\\u540e\\uff0c\\u9996\\u5148\\u5e94\\u5bf9\\u53d8\\u66f4\\u7684\\u8f6f\\u4ef6\\u5355\\u5143\\u8fdb\\u884c\\u6d4b\\u8bd5\\uff0c\\u7136\\u540e\\u518d\\u8fdb\\u884c\\u5176\\u4ed6\\u76f8\\u5173\\u7684\\u6d4b\\u8bd5\\u2002 C\\uff0e\\u8f6f\\u4ef6\\u53d8\\u66f4\\u540e\\uff0c\\u4e0d\\u5fc5\\u518d\\u5bf9\\u8f6f\\u4ef6\\u539f\\u6709\\u6b63\\u786e\\u7684\\u529f\\u80fd\\u3001\\u6027\\u80fd\\u548c\\u5176\\u4ed6\\u89c4\\u5b9a\\u7684\\u8981\\u6c42\\u8fdb\\u884c\\u6d4b\\u8bd5\\u2002 D\\uff0e\\u5bf9\\u5177\\u4f53\\u7684\\u8f6f\\u4ef6\\uff0c\\u53ef\\u4ee5\\u6839\\u636e\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u5408\\u540c\\u53ca\\u8f6f\\u4ef6\\u7684\\u91cd\\u8981\\u6027\\u3001\\u5b8c\\u6574\\u6027\\u7ea7\\u522b\\u5bf9\\u56de\\u5f52\\u6d4b\\u8bd5\\u5185\\u5bb9\\u8fdb\\u884c\\u526a\\u88c1\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=5\"}', '2020-08-25 07:48:12', '2020-08-25 07:48:12');
INSERT INTO `admin_operation_log` VALUES (365, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:48:13', '2020-08-25 07:48:13');
INSERT INTO `admin_operation_log` VALUES (366, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:48:14', '2020-08-25 07:48:14');
INSERT INTO `admin_operation_log` VALUES (367, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4ee5\\u4e0b\\u8bf4\\u6cd5\\u4e2d\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u8f6f\\u4ef6\\u914d\\u7f6e\\u9879\\u6d4b\\u8bd5\\u7684\\u76ee\\u7684\\u662f\\u68c0\\u9a8c\\u8f6f\\u4ef6\\u914d\\u7f6e\\u4e0e\\u8f6f\\u4ef6\\u9700\\u6c42\\u89c4\\u683c\\u8bf4\\u660e\\u7684\\u4e00\\u81f4\\u6027\\u2002B\\uff0e\\u8f6f\\u4ef6\\u914d\\u7f6e\\u9879\\u6d4b\\u8bd5\\u4e00\\u822c\\u7531\\u8f6f\\u4ef6\\u4f9b\\u65b9\\u7ec4\\u7ec7\\uff0c\\u7531\\u72ec\\u7acb\\u4e8e\\u8f6f\\u4ef6\\u5f00\\u53d1\\u7684\\u4eba\\u5458\\u5b9e\\u65bd\\uff0c\\u8f6f\\u4ef6\\u5f00\\u53d1\\u4eba\\u5458\\u914d\\u5408\\u2002 C\\uff0e\\u8f6f\\u4ef6\\u914d\\u7f6e\\u9879\\u6d4b\\u8bd5\\u4e0d\\u5f97\\u59d4\\u6258\\u7b2c\\u4e09\\u65b9\\u5b9e\\u65bd\\u2002 D\\uff0e\\u8f6f\\u4ef6\\u914d\\u7f6e\\u9879\\u6d4b\\u8bd5\\u8981\\u6c42\\u88ab\\u6d4b\\u8f6f\\u4ef6\\u914d\\u7f6e\\u9879\\u5df2\\u901a\\u8fc7\\u5355\\u5143\\u6d4b\\u8bd5\\u548c\\u96c6\\u6210\\u6d4b\\u8bd5\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:48:36', '2020-08-25 07:48:36');
INSERT INTO `admin_operation_log` VALUES (368, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:48:37', '2020-08-25 07:48:37');
INSERT INTO `admin_operation_log` VALUES (369, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:48:38', '2020-08-25 07:48:38');
INSERT INTO `admin_operation_log` VALUES (370, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u9762\\u8bf4\\u6cd5\\u6b63\\u786e\\u7684\\u662f\\u2002\\u3010C\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u7ecf\\u8fc7\\u6d4b\\u8bd5\\u6ca1\\u6709\\u53d1\\u73b0\\u9519\\u8bef\\u8bf4\\u660e\\u7a0b\\u5e8f\\u6b63\\u786e\\u2002B\\uff0e\\u6d4b\\u8bd5\\u7684\\u76ee\\u6807\\u662f\\u4e3a\\u4e86\\u8bc1\\u660e\\u7a0b\\u5e8f\\u6ca1\\u6709\\u9519\\u8bef\\u2002 C\\uff0e\\u6210\\u529f\\u7684\\u6d4b\\u8bd5\\u662f\\u53d1\\u73b0\\u4e86\\u8fc4\\u4eca\\u5c1a\\u672a\\u53d1\\u73b0\\u7684\\u9519\\u8bef\\u7684\\u6d4b\\u8bd5\\u2002D\\uff0e\\u6210\\u529f\\u7684\\u6d4b\\u8bd5\\u662f\\u6ca1\\u6709\\u53d1\\u73b0\\u9519\\u8bef\\u7684\\u6d4b\\u8bd5\\u2002\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\"}', '2020-08-25 07:48:56', '2020-08-25 07:48:56');
INSERT INTO `admin_operation_log` VALUES (371, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:48:56', '2020-08-25 07:48:56');
INSERT INTO `admin_operation_log` VALUES (372, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:48:57', '2020-08-25 07:48:57');
INSERT INTO `admin_operation_log` VALUES (373, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:48:59', '2020-08-25 07:48:59');
INSERT INTO `admin_operation_log` VALUES (374, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"5\"}', '2020-08-25 07:49:02', '2020-08-25 07:49:02');
INSERT INTO `admin_operation_log` VALUES (375, 1, 'admin/questions/98/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:49:06', '2020-08-25 07:49:06');
INSERT INTO `admin_operation_log` VALUES (376, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:49:07', '2020-08-25 07:49:07');
INSERT INTO `admin_operation_log` VALUES (377, 1, 'admin/questions/98', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u9762\\u8bf4\\u6cd5\\u6b63\\u786e\\u7684\\u662f\\u2002\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u7ecf\\u8fc7\\u6d4b\\u8bd5\\u6ca1\\u6709\\u53d1\\u73b0\\u9519\\u8bef\\u8bf4\\u660e\\u7a0b\\u5e8f\\u6b63\\u786e\\u2002|B\\uff0e\\u6d4b\\u8bd5\\u7684\\u76ee\\u6807\\u662f\\u4e3a\\u4e86\\u8bc1\\u660e\\u7a0b\\u5e8f\\u6ca1\\u6709\\u9519\\u8bef\\u2002 |C\\uff0e\\u6210\\u529f\\u7684\\u6d4b\\u8bd5\\u662f\\u53d1\\u73b0\\u4e86\\u8fc4\\u4eca\\u5c1a\\u672a\\u53d1\\u73b0\\u7684\\u9519\\u8bef\\u7684\\u6d4b\\u8bd5\\u2002|D\\uff0e\\u6210\\u529f\\u7684\\u6d4b\\u8bd5\\u662f\\u6ca1\\u6709\\u53d1\\u73b0\\u9519\\u8bef\\u7684\\u6d4b\\u8bd5\\u2002\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=5\"}', '2020-08-25 07:49:09', '2020-08-25 07:49:09');
INSERT INTO `admin_operation_log` VALUES (378, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\"}', '2020-08-25 07:49:10', '2020-08-25 07:49:10');
INSERT INTO `admin_operation_log` VALUES (379, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:49:47', '2020-08-25 07:49:47');
INSERT INTO `admin_operation_log` VALUES (380, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:49:48', '2020-08-25 07:49:48');
INSERT INTO `admin_operation_log` VALUES (381, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4ee5\\u4e0b\\u54ea\\u79cd\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u5c5e\\u4e8e\\u8f6f\\u4ef6\\u6027\\u80fd\\u6d4b\\u8bd5\\u7684\\u8303\\u7574\\u3010B\\u3011\\u3002\",\"options\":\"A\\uff0e\\u63a5\\u53e3\\u6d4b\\u8bd5\\u2002\\u2002\\u2002B\\uff0e\\u538b\\u529b\\u6d4b\\u8bd5\\u2002\\u2002\\u2002C\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u2002\\u2002\\u2002D\\uff0e\\u6613\\u7528\\u6027\\u6d4b\\u8bd5\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?&page=5\"}', '2020-08-25 07:50:07', '2020-08-25 07:50:07');
INSERT INTO `admin_operation_log` VALUES (382, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-25 07:50:08', '2020-08-25 07:50:08');
INSERT INTO `admin_operation_log` VALUES (383, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:50:09', '2020-08-25 07:50:09');
INSERT INTO `admin_operation_log` VALUES (384, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u7ba1\\u7406\\u5305\\u62ec\\u6d4b\\u8bd5\\u8fc7\\u7a0b\\u7ba1\\u7406\\u3001\\u914d\\u7f6e\\u7ba1\\u7406\\u4ee5\\u53ca\\u3010\\u3011\",\"options\":\"A\\uff0e\\u6d4b\\u8bd5\\u8bc4\\u5ba1\\u7ba1\\u7406\\u2002B\\uff0e\\u6d4b\\u8bd5\\u7528\\u4f8b\\u7ba1\\u7406\\u2002C\\uff0e\\u6d4b\\u8bd5\\u8ba1\\u5212\\u7ba1\\u7406\\u2002\\u2002D\\uff0e\\u6d4b\\u8bd5\\u5b9e\\u65bd\\u7ba1\\u7406\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\"}', '2020-08-25 07:50:33', '2020-08-25 07:50:33');
INSERT INTO `admin_operation_log` VALUES (385, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-25 07:50:34', '2020-08-25 07:50:34');
INSERT INTO `admin_operation_log` VALUES (386, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\",\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:51:01', '2020-08-25 07:51:01');
INSERT INTO `admin_operation_log` VALUES (387, 1, 'admin/questions/99/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 07:51:17', '2020-08-25 07:51:17');
INSERT INTO `admin_operation_log` VALUES (388, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-25 07:51:18', '2020-08-25 07:51:18');
INSERT INTO `admin_operation_log` VALUES (389, 1, 'admin/questions/99', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4ee5\\u4e0b\\u54ea\\u79cd\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u5c5e\\u4e8e\\u8f6f\\u4ef6\\u6027\\u80fd\\u6d4b\\u8bd5\\u7684\\u8303\\u7574\\u3010\\u3011\\u3002\",\"options\":\"A\\uff0e\\u63a5\\u53e3\\u6d4b\\u8bd5\\u2002\\u2002\\u2002|B\\uff0e\\u538b\\u529b\\u6d4b\\u8bd5\\u2002\\u2002\\u2002|C\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u2002\\u2002\\u2002|D\\uff0e\\u6613\\u7528\\u6027\\u6d4b\\u8bd5\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"lDTnOeN4hKpA2BqL2xCzdrUztodyie6FKaNEjxMk\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=5\"}', '2020-08-25 07:51:22', '2020-08-25 07:51:22');
INSERT INTO `admin_operation_log` VALUES (390, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\"}', '2020-08-25 07:51:22', '2020-08-25 07:51:22');
INSERT INTO `admin_operation_log` VALUES (391, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\"}', '2020-08-25 07:51:35', '2020-08-25 07:51:35');
INSERT INTO `admin_operation_log` VALUES (392, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"5\"}', '2020-08-25 09:12:03', '2020-08-25 09:12:03');
INSERT INTO `admin_operation_log` VALUES (393, 1, 'admin', 'GET', '192.168.1.100', '[]', '2020-08-26 03:30:30', '2020-08-26 03:30:30');
INSERT INTO `admin_operation_log` VALUES (394, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:30:40', '2020-08-26 03:30:40');
INSERT INTO `admin_operation_log` VALUES (395, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:30:45', '2020-08-26 03:30:45');
INSERT INTO `admin_operation_log` VALUES (396, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:30:46', '2020-08-26 03:30:46');
INSERT INTO `admin_operation_log` VALUES (397, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u3010\\u3011\\u7684\\u76ee\\u7684\\u662f\\u5bf9\\u6700\\u7ec8\\u8f6f\\u4ef6\\u7cfb\\u7edf\\u8fdb\\u884c\\u5168\\u9762\\u7684\\u6d4b\\u8bd5\\uff0c\\u786e\\u4fdd\\u6700\\u7ec8\\u8f6f\\u4ef6\\u7cfb\\u7edf\\u6ee1\\u8db3\\u4ea7\\u54c1\\u9700\\u6c42\\u5e76\\u4e14\\u9075\\u5faa\\u7cfb\\u7edf\\u8bbe\\u8ba1\\u3002\",\"options\":\"A\\uff0e\\u7cfb\\u7edf\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u96c6\\u6210\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u529f\\u80fd\\u6d4b\\u8bd5\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-26 03:31:27', '2020-08-26 03:31:27');
INSERT INTO `admin_operation_log` VALUES (398, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:31:28', '2020-08-26 03:31:28');
INSERT INTO `admin_operation_log` VALUES (399, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:31:29', '2020-08-26 03:31:29');
INSERT INTO `admin_operation_log` VALUES (400, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5982\\u679c\\u4e00\\u4e2a\\u4ea7\\u54c1\\u4e2d\\u6b21\\u4e25\\u91cd\\u7684\\u7f3a\\u9677\\u57fa\\u672c\\u5b8c\\u6210\\u4fee\\u6b63\\u5e76\\u901a\\u8fc7\\u590d\\u6d4b\\uff0c\\u8fd9\\u4e2a\\u9636\\u6bb5\\u7684\\u6210\\u54c1\\u662f\\u3010\\u3011\\u3002\",\"options\":\"A\\uff0eAlpha\\u7248\\u2002B\\uff0eBeta\\u7248\\u2002C\\uff0e\\u6b63\\u7248\\u2002D\\uff0e\\u4ee5\\u4e0a\\u90fd\\u4e0d\\u662f\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:32:02', '2020-08-26 03:32:02');
INSERT INTO `admin_operation_log` VALUES (401, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:32:02', '2020-08-26 03:32:02');
INSERT INTO `admin_operation_log` VALUES (402, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:32:03', '2020-08-26 03:32:03');
INSERT INTO `admin_operation_log` VALUES (403, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5de5\\u5177\\u4e2d\\u53ef\\u4ee5\\u76f4\\u63a5\\u8fde\\u63a5mysql\\u7684\\u5de5\\u5177\\u6709\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0exsell\\u2002\\u2002\\u2002\\u2002B\\uff0eplsql\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0enavicat\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u4ee5\\u4e0a\\u90fd\\u4e0d\\u662f\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:32:27', '2020-08-26 03:32:27');
INSERT INTO `admin_operation_log` VALUES (404, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:32:28', '2020-08-26 03:32:28');
INSERT INTO `admin_operation_log` VALUES (405, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:32:29', '2020-08-26 03:32:29');
INSERT INTO `admin_operation_log` VALUES (406, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5fc5\\u987b\\u8981\\u6c42\\u7528\\u6237\\u53c2\\u4e0e\\u7684\\u6d4b\\u8bd5\\u9636\\u6bb5\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u96c6\\u6210\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u786e\\u8ba4\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u9a8c\\u6536\\u6d4b\\u8bd5\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:32:48', '2020-08-26 03:32:48');
INSERT INTO `admin_operation_log` VALUES (407, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:32:48', '2020-08-26 03:32:48');
INSERT INTO `admin_operation_log` VALUES (408, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:32:49', '2020-08-26 03:32:49');
INSERT INTO `admin_operation_log` VALUES (409, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5bf9Web\\u7f51\\u7ad9\\u8fdb\\u884c\\u7684\\u6d4b\\u8bd5\\u4e2d\\uff0c\\u5c5e\\u4e8e\\u529f\\u80fd\\u6d4b\\u8bd5\\u7684\\u662f\\u3010\\u3011\\u2002\",\"options\":\"A\\uff0e\\u8fde\\u63a5\\u901f\\u5ea6\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u94fe\\u63a5\\u6d4b\\u8bd5\\u2002\\u2002\\u2002C\\uff0e\\u5e73\\u53f0\\u6d4b\\u8bd5\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u5b89\\u5168\\u6027\\u6d4b\\u8bd5\\u2002\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\"}', '2020-08-26 03:33:02', '2020-08-26 03:33:02');
INSERT INTO `admin_operation_log` VALUES (410, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-26 03:33:03', '2020-08-26 03:33:03');
INSERT INTO `admin_operation_log` VALUES (411, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:33:12', '2020-08-26 03:33:12');
INSERT INTO `admin_operation_log` VALUES (412, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:33:13', '2020-08-26 03:33:13');
INSERT INTO `admin_operation_log` VALUES (413, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u3010\\u3011\\u4e0d\\u662f\\u8f6f\\u4ef6\\u81ea\\u52a8\\u5316\\u6d4b\\u8bd5\\u7684\\u4f18\\u70b9\\u3002\\u2002\",\"options\":\"A\\uff0e\\u901f\\u5ea6\\u5feb\\u3001\\u6548\\u7387\\u9ad8B\\uff0e\\u51c6\\u786e\\u5ea6\\u548c\\u7cbe\\u786e\\u5ea6\\u9ad8\\u2002C\\uff0e\\u80fd\\u63d0\\u9ad8\\u6d4b\\u8bd5\\u7684\\u8d28\\u91cfD\\uff0e\\u80fd\\u5145\\u5206\\u6d4b\\u8bd5\\u8f6f\\u4ef6\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-26 03:33:41', '2020-08-26 03:33:41');
INSERT INTO `admin_operation_log` VALUES (414, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:33:42', '2020-08-26 03:33:42');
INSERT INTO `admin_operation_log` VALUES (415, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:33:43', '2020-08-26 03:33:43');
INSERT INTO `admin_operation_log` VALUES (416, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u914d\\u7f6e\\u6d4b\\u8bd5\\u662f\\u6307\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u68c0\\u67e5\\u8f6f\\u4ef6\\u4e4b\\u95f4\\u662f\\u5426\\u6b63\\u786e\\u4ea4\\u4e92\\u548c\\u5171\\u4eab\\u4fe1\\u606f\\u2002\\u2002\\u2002B\\uff0e\\u4ea4\\u4e92\\u9002\\u5e94\\u6027\\u3001\\u5b9e\\u7528\\u6027\\u548c\\u6709\\u6548\\u6027\\u7684\\u96c6\\u4e2d\\u4f53\\u73b0\\u2002 C\\uff0e\\u4f7f\\u7528\\u5404\\u79cd\\u786c\\u4ef6\\u6765\\u6d4b\\u8bd5\\u8f6f\\u4ef6\\u64cd\\u4f5c\\u7684\\u8fc7\\u7a0b\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u68c0\\u67e5\\u7f3a\\u9677\\u662f\\u5426\\u6709\\u6548\\u6539\\u6b63\\u2002\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:34:06', '2020-08-26 03:34:06');
INSERT INTO `admin_operation_log` VALUES (417, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:34:06', '2020-08-26 03:34:06');
INSERT INTO `admin_operation_log` VALUES (418, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:34:07', '2020-08-26 03:34:07');
INSERT INTO `admin_operation_log` VALUES (419, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u4e0d\\u5c5e\\u4e8e\\u6d4b\\u8bd5\\u539f\\u5219\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u662f\\u6709\\u98ce\\u9669\\u7684\\u884c\\u4e3a\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002B\\uff0e\\u5b8c\\u5168\\u6d4b\\u8bd5\\u7a0b\\u5e8f\\u662f\\u4e0d\\u53ef\\u80fd\\u7684\\u2002 C\\uff0e\\u6d4b\\u8bd5\\u65e0\\u6cd5\\u663e\\u793a\\u6f5c\\u4f0f\\u7684\\u8f6f\\u4ef6\\u7f3a\\u9677\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u627e\\u5230\\u7684\\u7f3a\\u9677\\u8d8a\\u591a\\u8f6f\\u4ef6\\u7684\\u7f3a\\u9677\\u5c31\\u8d8a\\u5c11\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:34:22', '2020-08-26 03:34:22');
INSERT INTO `admin_operation_log` VALUES (420, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:34:23', '2020-08-26 03:34:23');
INSERT INTO `admin_operation_log` VALUES (421, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:34:24', '2020-08-26 03:34:24');
INSERT INTO `admin_operation_log` VALUES (422, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5404\\u9879\\u4e2d\\u3010\\u3011\\u4e0d\\u662f\\u4e00\\u4e2a\\u6d4b\\u8bd5\\u8ba1\\u5212\\u6240\\u5e94\\u5305\\u542b\\u7684\\u5185\\u5bb9\\u3002\",\"options\":\"A\\uff0e\\u6d4b\\u8bd5\\u8d44\\u6e90\\u3001\\u8fdb\\u5ea6\\u5b89\\u6392\\u2002\\u2002\\u2002B\\uff0e\\u6d4b\\u8bd5\\u9884\\u671f\\u8f93\\u51fa\\u2002\\u2002\\u2002C\\uff0e\\u6d4b\\u8bd5\\u8303\\u56f4\\u2002\\u2002D\\uff0e\\u6d4b\\u8bd5\\u7b56\\u7565\\u2002\",\"answer\":\"D\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\"}', '2020-08-26 03:34:40', '2020-08-26 03:34:40');
INSERT INTO `admin_operation_log` VALUES (423, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-26 03:34:41', '2020-08-26 03:34:41');
INSERT INTO `admin_operation_log` VALUES (424, 1, 'admin/questions/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:34:47', '2020-08-26 03:34:47');
INSERT INTO `admin_operation_log` VALUES (425, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:34:48', '2020-08-26 03:34:48');
INSERT INTO `admin_operation_log` VALUES (426, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u8c03\\u8bd5\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u53d1\\u73b0\\u4e0e\\u9884\\u5148\\u5b9a\\u4e49\\u7684\\u89c4\\u683c\\u548c\\u6807\\u51c6\\u4e0d\\u7b26\\u5408\\u7684\\u95ee\\u9898\\u2002B\\uff0e\\u53d1\\u73b0\\u8f6f\\u4ef6\\u9519\\u8bef\\u5f81\\u5146\\u7684\\u8fc7\\u7a0bC\\uff0e\\u6709\\u8ba1\\u5212\\u7684\\u3001\\u53ef\\u91cd\\u590d\\u7684\\u8fc7\\u7a0b\\u2002D\\uff0e\\u6d88\\u9664\\u8f6f\\u4ef6\\u9519\\u8bef\\u7684\\u8fc7\\u7a0b\\u2002\",\"answer\":\"B\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions\"}', '2020-08-26 03:35:01', '2020-08-26 03:35:01');
INSERT INTO `admin_operation_log` VALUES (427, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:35:02', '2020-08-26 03:35:02');
INSERT INTO `admin_operation_log` VALUES (428, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:35:03', '2020-08-26 03:35:03');
INSERT INTO `admin_operation_log` VALUES (429, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u63cf\\u8ff0\\u9519\\u8bef\\u7684\\u662f\\u3010\\u3011\\u3002\",\"options\":\"A\\uff0e\\u8f6f\\u4ef6\\u53d1\\u5e03\\u540e\\u5982\\u679c\\u53d1\\u73b0\\u8d28\\u91cf\\u95ee\\u9898\\uff0c\\u90a3\\u662f\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u4eba\\u5458\\u7684\\u9519\\u2002B\\uff0e\\u7a77\\u5c3d\\u6d4b\\u8bd5\\u5b9e\\u9645\\u4e0a\\u5728\\u4e00\\u822c\\u60c5\\u51b5\\u4e0b\\u662f\\u4e0d\\u53ef\\u884c\\u7684\\u2002C\\uff0e\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u81ea\\u52a8\\u5316\\u4e0d\\u662f\\u4e07\\u80fd\\u7684\\u2002 D\\uff0e\\u6d4b\\u8bd5\\u80fd\\u7531\\u975e\\u5f00\\u53d1\\u4eba\\u5458\\u8fdb\\u884c\\uff0c\\u8c03\\u8bd5\\u5fc5\\u987b\\u7531\\u5f00\\u53d1\\u4eba\\u5458\\u8fdb\\u884c\\u3002\\u2002\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:35:21', '2020-08-26 03:35:21');
INSERT INTO `admin_operation_log` VALUES (430, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:35:22', '2020-08-26 03:35:22');
INSERT INTO `admin_operation_log` VALUES (431, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:35:23', '2020-08-26 03:35:23');
INSERT INTO `admin_operation_log` VALUES (432, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5728\\u8f6f\\u4ef6\\u4fee\\u6539\\u4e4b\\u540e\\uff0c\\u518d\\u6b21\\u8fd0\\u884c\\u4ee5\\u524d\\u4e3a\\u53d1\\u73b0\\u9519\\u8bef\\u800c\\u6267\\u884c\\u7a0b\\u5e8f\\u66fe\\u7528\\u8fc7\\u7684\\u6d4b\\u8bd5\\u7528\\u4f8b\\uff0c\\u8fd9\\u79cd\\u6d4b\\u8bd5\\u79f0\\u4e4b\\u4e3a\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0e\\u5355\\u5143\\u6d4b\\u8bd5\\u2002B\\uff0e\\u96c6\\u6210\\u6d4b\\u8bd5\\u2002C\\uff0e\\u56de\\u5f52\\u6d4b\\u8bd5\\u2002D\\uff0e\\u9a8c\\u6536\\u6d4b\\u8bd5\\u2002\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:35:36', '2020-08-26 03:35:36');
INSERT INTO `admin_operation_log` VALUES (433, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:35:37', '2020-08-26 03:35:37');
INSERT INTO `admin_operation_log` VALUES (434, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:35:37', '2020-08-26 03:35:37');
INSERT INTO `admin_operation_log` VALUES (435, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u5728\\u4e0b\\u9762\\u6240\\u5217\\u4e3e\\u4e2d\\u7684\\u903b\\u8f91\\u6d4b\\u8bd5\\u8986\\u76d6\\u4e2d\\uff0c\\u6d4b\\u8bd5\\u8986\\u76d6\\u6700\\u5f3a\\u7684\\u662f\\u3010\\u3011\\u3002\",\"options\":\"A. \\u6761\\u4ef6\\u8986\\u76d6\\u2002B\\uff0e\\u6761\\u4ef6\\u7ec4\\u5408\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002C\\uff0e\\u8bed\\u53e5\\u8986\\u76d6\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002\\u2002D\\uff0e\\u5224\\u5b9a\\u8986\\u76d6\",\"answer\":\"C\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"after-save\":\"2\"}', '2020-08-26 03:36:05', '2020-08-26 03:36:05');
INSERT INTO `admin_operation_log` VALUES (436, 1, 'admin/questions/create', 'GET', '192.168.1.100', '[]', '2020-08-26 03:36:05', '2020-08-26 03:36:05');
INSERT INTO `admin_operation_log` VALUES (437, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:36:06', '2020-08-26 03:36:06');
INSERT INTO `admin_operation_log` VALUES (438, 1, 'admin/questions', 'POST', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5173\\u4e8eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u7684\\u8bf4\\u6cd5\\u4e2d\\uff0c\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0eCookie\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u529f\\u80fd\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 B\\uff0e\\u5bf9\\u4e8e\\u6ca1\\u6709\\u4f7f\\u7528\\u6570\\u636e\\u5e93\\u7684Web\\u5e94\\u7528\\u8f6f\\u4ef6\\uff0c\\u4e0d\\u9700\\u8981\\u8fdb\\u884c\\u6027\\u80fd\\u6d4b\\u8bd5\\u2002C\\uff0e\\u94fe\\u63a5\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6613\\u7528\\u6027\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 D\\uff0eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u5b89\\u5168\\u6027\\u6d4b\\u8bd5\\u4ec5\\u5173\\u6ce8Web\\u5e94\\u7528\\u8f6f\\u4ef6\\u662f\\u80fd\\u591f\\u9632\\u5fa1\\u7f51\\u7edc\\u653b\\u51fb\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\"}', '2020-08-26 03:36:25', '2020-08-26 03:36:25');
INSERT INTO `admin_operation_log` VALUES (439, 1, 'admin/questions', 'GET', '192.168.1.100', '[]', '2020-08-26 03:36:26', '2020-08-26 03:36:26');
INSERT INTO `admin_operation_log` VALUES (440, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"6\",\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:36:31', '2020-08-26 03:36:31');
INSERT INTO `admin_operation_log` VALUES (441, 1, 'admin/questions/114/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:38:34', '2020-08-26 03:38:34');
INSERT INTO `admin_operation_log` VALUES (442, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:38:35', '2020-08-26 03:38:35');
INSERT INTO `admin_operation_log` VALUES (443, 1, 'admin/questions/114', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5173\\u4e8eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u7684\\u8bf4\\u6cd5\\u4e2d\\uff0c\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0eCookie\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u529f\\u80fd\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 |B\\uff0e\\u5bf9\\u4e8e\\u6ca1\\u6709\\u4f7f\\u7528\\u6570\\u636e\\u5e93\\u7684Web\\u5e94\\u7528\\u8f6f\\u4ef6\\uff0c\\u4e0d\\u9700\\u8981\\u8fdb\\u884c\\u6027\\u80fd\\u6d4b\\u8bd5\\u2002|C\\uff0e\\u94fe\\u63a5\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6613\\u7528\\u6027\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 |D\\uff0eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u5b89\\u5168\\u6027\\u6d4b\\u8bd5\\u4ec5\\u5173\\u6ce8Web\\u5e94\\u7528\\u8f6f\\u4ef6\\u662f\\u80fd\\u591f\\u9632\\u5fa1\\u7f51\\u7edc\\u653b\\u51fb\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=6\"}', '2020-08-26 03:38:50', '2020-08-26 03:38:50');
INSERT INTO `admin_operation_log` VALUES (444, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"6\"}', '2020-08-26 03:38:51', '2020-08-26 03:38:51');
INSERT INTO `admin_operation_log` VALUES (445, 1, 'admin/questions/114/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:38:59', '2020-08-26 03:38:59');
INSERT INTO `admin_operation_log` VALUES (446, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:39:00', '2020-08-26 03:39:00');
INSERT INTO `admin_operation_log` VALUES (447, 1, 'admin/questions/114', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5173\\u4e8eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u7684\\u8bf4\\u6cd5\\u4e2d\\uff0c\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0ecookie\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u529f\\u80fd\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 |B\\uff0e\\u5bf9\\u4e8e\\u6ca1\\u6709\\u4f7f\\u7528\\u6570\\u636e\\u5e93\\u7684Web\\u5e94\\u7528\\u8f6f\\u4ef6\\uff0c\\u4e0d\\u9700\\u8981\\u8fdb\\u884c\\u6027\\u80fd\\u6d4b\\u8bd5\\u2002C\\uff0e\\u94fe\\u63a5\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6613\\u7528\\u6027\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 |D\\uff0eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u5b89\\u5168\\u6027\\u6d4b\\u8bd5\\u4ec5\\u5173\\u6ce8Web\\u5e94\\u7528\\u8f6f\\u4ef6\\u662f\\u80fd\\u591f\\u9632\\u5fa1\\u7f51\\u7edc\\u653b\\u51fb\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=6\"}', '2020-08-26 03:39:16', '2020-08-26 03:39:16');
INSERT INTO `admin_operation_log` VALUES (448, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"6\"}', '2020-08-26 03:39:17', '2020-08-26 03:39:17');
INSERT INTO `admin_operation_log` VALUES (449, 1, 'admin/questions/114/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:39:25', '2020-08-26 03:39:25');
INSERT INTO `admin_operation_log` VALUES (450, 1, 'admin/api/course', 'GET', '192.168.1.100', '[]', '2020-08-26 03:39:26', '2020-08-26 03:39:26');
INSERT INTO `admin_operation_log` VALUES (451, 1, 'admin/questions/114', 'PUT', '192.168.1.100', '{\"course_id\":\"2\",\"question\":\"\\u4e0b\\u5217\\u5173\\u4e8eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6d4b\\u8bd5\\u7684\\u8bf4\\u6cd5\\u4e2d\\uff0c\\u6b63\\u786e\\u7684\\u662f\\u3010\\u3011\\u3002\\u2002\",\"options\":\"A\\uff0ecookie\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u529f\\u80fd\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 |B\\uff0e\\u5bf9\\u4e8e\\u6ca1\\u6709\\u4f7f\\u7528\\u6570\\u636e\\u5e93\\u7684Web\\u5e94\\u7528\\u8f6f\\u4ef6\\uff0c\\u4e0d\\u9700\\u8981\\u8fdb\\u884c\\u6027\\u80fd\\u6d4b\\u8bd5\\u2002C\\uff0e\\u94fe\\u63a5\\u6d4b\\u8bd5\\u662fWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u6613\\u7528\\u6027\\u6d4b\\u8bd5\\u7684\\u91cd\\u8981\\u5185\\u5bb9\\u2002 |D\\uff0eWeb\\u5e94\\u7528\\u8f6f\\u4ef6\\u5b89\\u5168\\u6027\\u6d4b\\u8bd5\\u4ec5\\u5173\\u6ce8Web\\u5e94\\u7528\\u8f6f\\u4ef6\\u662f\\u80fd\\u591f\\u9632\\u5fa1\\u7f51\\u7edc\\u653b\\u51fb\",\"answer\":\"A\",\"analysis\":null,\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/questions?page=6\"}', '2020-08-26 03:39:49', '2020-08-26 03:39:49');
INSERT INTO `admin_operation_log` VALUES (452, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"6\"}', '2020-08-26 03:39:50', '2020-08-26 03:39:50');
INSERT INTO `admin_operation_log` VALUES (453, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"6\"}', '2020-08-26 03:40:28', '2020-08-26 03:40:28');
INSERT INTO `admin_operation_log` VALUES (454, 1, 'admin/majors', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:58:30', '2020-08-26 03:58:30');
INSERT INTO `admin_operation_log` VALUES (455, 1, 'admin/courses', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:58:32', '2020-08-26 03:58:32');
INSERT INTO `admin_operation_log` VALUES (456, 1, 'admin/courses/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:58:35', '2020-08-26 03:58:35');
INSERT INTO `admin_operation_log` VALUES (457, 1, 'admin/courses', 'POST', '192.168.1.100', '{\"major_id\":\"0\",\"code\":\"07029\",\"name\":\"\\u8f6f\\u4ef6\\u9879\\u76ee\\u7ba1\\u7406\",\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/courses\"}', '2020-08-26 03:58:53', '2020-08-26 03:58:53');
INSERT INTO `admin_operation_log` VALUES (458, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-08-26 03:58:54', '2020-08-26 03:58:54');
INSERT INTO `admin_operation_log` VALUES (459, 1, 'admin/courses/create', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:59:12', '2020-08-26 03:59:12');
INSERT INTO `admin_operation_log` VALUES (460, 1, 'admin/courses', 'POST', '192.168.1.100', '{\"major_id\":null,\"code\":\"07026\",\"name\":\"\\u7f51\\u7edc\\u5e94\\u7528\\u7a0b\\u5e8f\\u8bbe\\u8ba1\",\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/courses\"}', '2020-08-26 03:59:21', '2020-08-26 03:59:21');
INSERT INTO `admin_operation_log` VALUES (461, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-08-26 03:59:22', '2020-08-26 03:59:22');
INSERT INTO `admin_operation_log` VALUES (462, 1, 'admin/courses/3/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:59:38', '2020-08-26 03:59:38');
INSERT INTO `admin_operation_log` VALUES (463, 1, 'admin/courses/3', 'PUT', '192.168.1.100', '{\"major_id\":\"1\",\"code\":\"07029\",\"name\":\"\\u8f6f\\u4ef6\\u9879\\u76ee\\u7ba1\\u7406\",\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/courses\"}', '2020-08-26 03:59:42', '2020-08-26 03:59:42');
INSERT INTO `admin_operation_log` VALUES (464, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-08-26 03:59:43', '2020-08-26 03:59:43');
INSERT INTO `admin_operation_log` VALUES (465, 1, 'admin/courses/4/edit', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 03:59:46', '2020-08-26 03:59:46');
INSERT INTO `admin_operation_log` VALUES (466, 1, 'admin/courses/4', 'PUT', '192.168.1.100', '{\"major_id\":\"1\",\"code\":\"07026\",\"name\":\"\\u7f51\\u7edc\\u5e94\\u7528\\u7a0b\\u5e8f\\u8bbe\\u8ba1\",\"_token\":\"JDnW43DAtRLF5ExMQEqpdXHYlEsECd0o5LYUp6Oa\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/self-quiz.com\\/admin\\/courses\"}', '2020-08-26 03:59:50', '2020-08-26 03:59:50');
INSERT INTO `admin_operation_log` VALUES (467, 1, 'admin/courses', 'GET', '192.168.1.100', '[]', '2020-08-26 03:59:50', '2020-08-26 03:59:50');
INSERT INTO `admin_operation_log` VALUES (468, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\"}', '2020-08-26 06:39:53', '2020-08-26 06:39:53');
INSERT INTO `admin_operation_log` VALUES (469, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"_pjax\":\"#pjax-container\",\"page\":\"7\"}', '2020-08-26 06:39:58', '2020-08-26 06:39:58');
INSERT INTO `admin_operation_log` VALUES (470, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"7\"}', '2020-08-26 07:18:10', '2020-08-26 07:18:10');
INSERT INTO `admin_operation_log` VALUES (471, 1, 'admin/questions', 'GET', '192.168.1.100', '{\"page\":\"12\",\"_pjax\":\"#pjax-container\"}', '2020-08-26 07:18:16', '2020-08-26 07:18:16');

-- ----------------------------
-- Table structure for admin_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE `admin_permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `http_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `http_path` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_permissions_name_unique`(`name`) USING BTREE,
  UNIQUE INDEX `admin_permissions_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_permissions
-- ----------------------------
INSERT INTO `admin_permissions` VALUES (1, 'All permission', '*', '', '*', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

-- ----------------------------
-- Table structure for admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu`  (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_role_menu_role_id_menu_id_index`(`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES (1, 2, NULL, NULL);

-- ----------------------------
-- Table structure for admin_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions`  (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_role_permissions_role_id_permission_id_index`(`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_permissions
-- ----------------------------
INSERT INTO `admin_role_permissions` VALUES (1, 1, NULL, NULL);

-- ----------------------------
-- Table structure for admin_role_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE `admin_role_users`  (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_role_users_role_id_user_id_index`(`role_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_users
-- ----------------------------
INSERT INTO `admin_role_users` VALUES (1, 1, NULL, NULL);

-- ----------------------------
-- Table structure for admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_roles_name_unique`(`name`) USING BTREE,
  UNIQUE INDEX `admin_roles_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles` VALUES (1, 'Administrator', 'administrator', '2020-07-22 03:51:46', '2020-07-22 03:51:46');

-- ----------------------------
-- Table structure for admin_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE `admin_user_permissions`  (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_user_permissions_user_id_permission_id_index`(`user_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for admin_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES (1, 'admin', '$2y$10$v/th8z1EzkZmTF0ZYv6g0Or6YR5DGNmGTKVvjZCJfplmGeYGv9GBC', 'Administrator', NULL, '7FjydsOxS0Q9hcpUmb6bjdDkDRTvVEPTRybWs9CP46g5EG6AiWuuBQrJk7IS', '2020-07-22 03:51:46', '2020-07-22 03:51:46');

-- ----------------------------
-- Table structure for courses
-- ----------------------------
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `major_id` int(11) NULL DEFAULT NULL COMMENT '所属专业',
  `code` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of courses
-- ----------------------------
INSERT INTO `courses` VALUES (1, 1, '02333', '软件工程');
INSERT INTO `courses` VALUES (2, 1, '07028', '软件测试技术');
INSERT INTO `courses` VALUES (3, 1, '07029', '软件项目管理');
INSERT INTO `courses` VALUES (4, 1, '07026', '网络应用程序设计');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `queue` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for majors
-- ----------------------------
DROP TABLE IF EXISTS `majors`;
CREATE TABLE `majors`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of majors
-- ----------------------------
INSERT INTO `majors` VALUES (1, '080720', '软件工程 [独立本科段]');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (16, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (17, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (18, '2016_01_04_173148_create_admin_tables', 1);
INSERT INTO `migrations` VALUES (19, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (20, '2020_07_21_060719_create_major_table', 1);
INSERT INTO `migrations` VALUES (21, '2020_07_21_064747_create_courses_table', 1);
INSERT INTO `migrations` VALUES (22, '2020_07_21_064812_create_questions_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_12_14_000001_create_personal_access_tokens_table', 2);
INSERT INTO `migrations` VALUES (24, '2020_08_20_082036_add_analysis_to_questions_table', 3);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `abilities` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
INSERT INTO `personal_access_tokens` VALUES (1, 'App\\User', 1, 'my-app-token', 'd4cb5bd18fdab96978587ddc8fe9ef5b1df32509755fc00b689e0c1e9324e738', '[\"*\"]', NULL, '2020-08-17 08:18:17', '2020-08-17 08:18:17');
INSERT INTO `personal_access_tokens` VALUES (2, 'App\\User', 1, 'my-app-token', 'fe9128b104b00de0fc10b2d3ed9911f04777bb465563634030a76dfa7ca3ce3f', '[\"*\"]', NULL, '2020-08-17 08:18:23', '2020-08-17 08:18:23');
INSERT INTO `personal_access_tokens` VALUES (3, 'App\\User', 1, 'my-app-token', 'a92a40e2de4170c7b6802cdd61e8510af3ab1c111d290a4ff81c7aabe541ac45', '[\"*\"]', NULL, '2020-08-17 09:09:55', '2020-08-17 09:09:55');
INSERT INTO `personal_access_tokens` VALUES (4, 'App\\User', 1, 'my-app-token', 'fcb10a4639337a51d04fe3505129faa0edb185c80a36391e7855b11b931b027c', '[\"*\"]', '2020-08-19 09:07:46', '2020-08-17 09:18:26', '2020-08-19 09:07:46');

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NULL DEFAULT NULL COMMENT '所属课程',
  `question` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题描述',
  `options` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '题目选项',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正确答案',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `analysis` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '答案解析',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 222 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, 1, '软件是一种逻辑产品，它的开发主要是（）', 'A，研制|B，拷贝|C，再生产|D，复制', 'A', '2020-08-14 09:16:26', '2020-08-14 09:16:26', '暂无');
INSERT INTO `questions` VALUES (2, 1, '软件生命周期一般包括：软件开发期和软件运行期，下述（）不是软件开发期所应包含的内容。', 'A，需求分析|B，结构设计|C，程序编制|D，软件维护', 'D', '2020-08-14 09:21:43', '2020-08-14 09:21:43', '暂无');
INSERT INTO `questions` VALUES (3, 1, '以文档作为驱动，适合于软件需求很明确的软件项目的生存周期模型是（）。', 'A，喷泉模型|B，增量模型|C，瀑布模型|D，螺旋模型', 'C', '2020-08-14 09:24:22', '2020-08-20 09:19:50', '暂无');
INSERT INTO `questions` VALUES (4, 1, '在软件生存周期中，（）阶段必须要回答的问题是“要解决的问题是做什么？\"', 'A，详细设计|B，可行性分析和项目开发计划|C，概要设计|D，软件测试', 'B', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (5, 1, '软件产品与物质产品有很大区别，软件产品是一种（ ）产品', 'A，有形|B，消耗|C，逻辑|D，文档', 'C', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (6, 1, '（ ）把瀑布模型和专家系统结合在一起，在开发的各个阶段上都利用相应的专家系统来帮助软件人员完成开发工作。', 'A，原型模型|B，螺旋模型|C，基于知识的智能模型|D，喷泉模型', 'C', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (7, 1, '（ ）阶段是为每个模块完成的功能进行具体的描述，要把功能描述转变为精确的、结构化的过程描述。', 'A，概要设计|B，详细设计|C，编码|D，测试', 'B', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (8, 1, '下列软件开发模型中，适合于那些不能预先确切定义需求的软件系统的开发的模型是（ ）。', 'A，原型模型|B，瀑布模型|C，基于知识的智能模型|D，变换模型', 'A', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (9, 1, '下列软件开发模型中，以面向对象的软件开发方法为基础，以用户的需求为动力，以对象来驱动的模型是（ ）。', 'A，原型模型|B，瀑布模型|C，喷泉模型|D，螺旋模型', 'C', '2020-08-20 09:23:20', '2020-08-24 05:39:36', '暂无');
INSERT INTO `questions` VALUES (10, 1, '下列软件开发模型中，支持需求不明确，特别是大型软件系统的开发，并支持多种软件开发方法的模型是（ ）。', 'A，原型模型|B，瀑布模型|C，喷泉模型|D，螺旋模型', 'D', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (11, 1, '软件特性中，使软件在不同的系统约束条件下，使用户需求得到满足的难易程度称为（ ）。', 'A，可修改性|B，可靠性|C，可适应性|D，可重用性', 'C', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (12, 1, '软件特性中，一个软件能再次用于其他相关应用的程度称为（ ）。', 'A，可移植性|B，可重用性|C，容错性|D，可适应性', 'B', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (13, 1, '软件特性中，（ ）是指系统具有清晰的结构，能直接反映问题的需求的程度。', 'A，可理解性|B，可靠性|C，可适应性|D，可重用性', 'A', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (14, 1, '软件特性中，软件产品交付使用后，在实现改正潜伏的错误、改进性能、适应环境变化等方面工作的难易程度称为（ ）。', 'A，可理解性|B，可维护性|C，可适应性|D，可重用性', 'B', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (15, 1, '软件特性中，软件从一个计算机系统或环境移植到另一个上去的难易程度指的是（ ）.', 'A，可理解性|B，可修改性|C，可移植性|D，可重用性', 'C', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (16, 1, '软件特性中，在给定的时间间隔内，程序成功运行的概率指的是（ ）。', 'A，有效性|B，可适应性|C，正确性|D，可靠性', 'D', '2020-08-20 09:23:20', '2020-08-20 09:23:34', '暂无');
INSERT INTO `questions` VALUES (17, 1, '软件特性中，允许对软件进行修改而不增加其复杂性指的是（ ）。', 'A，可修改性|B，可适应性|C，可维护性|D，可移植性', ' A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (18, 1, '软件特性中，多个软件元素相互通讯并协同完成任务的能力指的是（ ）。', 'A，可理解性|B，可互操作性|C，可维护性|D，可追踪性', ' B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (19, 1, '软件特性中，根据软件需求对软件设计、程序进行正向追踪，或根据程序、软件设计对软件需求进行逆向追踪的能力指的是（ ）。', 'A，可理解性|B，可互操作性|C，可追踪性|D，可维护性', ' C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (20, 1, '软件的可修改性支持软件的（ ）。', 'A，有效性|B，可互操作性|C，可追踪性|D，可维护性', ' D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (21, 1, '软件的可移植性支持软件的（ ）。', 'A，可适应性|B，可互操作性|C，可追踪性|D，有效性', ' A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (22, 1, '软件的可理解性支持软件的（ ）。', 'A，有效性|B，可移植性|C，可追踪性|D，可靠性', ' B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (23, 1, '在软件工程的原则中，抽象、信息隐藏、模块化和局部化的原则支持软件的（ ）。', 'A，有效性|B，可互操作性|C，可靠性|D，可追踪性', ' C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (24, 1, '下列选项中，属于需求分析阶段的任务的是（ ）。', 'A，组装测试计划|B，单元测试计划|C，软件总体设计|D，验收测试计划', ' D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (25, 1, '下列选项中，属于概要设计阶段的任务的是（ ）。', 'A，组装测试计划|B，单元测试计划|C，初步用户手册|D，验收测试计划', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (26, 1, '下列选项中，属于详细设计阶段的任务的是（ ）。', 'A，组装测试计划|B，单元测试计划|C，初步用户手册|D，验收测试计划', ' B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (27, 1, '下列选项中，属于实现阶段的任务的是（ ）。', 'A，组装测试计划|B，绘制程序流程图|C，单元测试|D，验收测试计划', ' C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (28, 1, '下列选项中，在验收测试结束时，必须提交给用户的是（ ）。', 'A，项目开发总结报告|B，验收测试计划|C，需求规格说明书|D，最终用户手册', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (29, 1, '软件工程的概念是哪年提出的( )。', 'A.1988|B.1968|C.1948|D.1928', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (30, 1, '瀑布模型的关键不足在于( )。', 'A.过于简单|B.各个阶段需要进行评审|C.过于灵活|D.不能适应需求的动态变更', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '[考点] 本题主要考查的知识点为瀑布模型。<br>多年来，瀑布模型得以广泛流行，这是因为它在支持结构化软件开发、控制软件开发的复杂性、促进软件开发工程等方面起着很大作用。瀑布模型也有其内在问题，主要表现为无法通过开发活动澄清本来不够确切的软件工作。');
INSERT INTO `questions` VALUES (31, 1, '以下哪一项不是软件危机的表现形式( )。', 'A.开发的软件不满足用户需要|B.开发的软件可维护性差|C.开发的软件价格便宜|D.开发的软件可靠性差', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (32, 1, '软件可行性研究实质上是要进行一次( )需求分析、设计过程。', 'A.简化、压缩的|B.详细的|C.彻底的|D.深入的', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (33, 1, '结构化设计是一种面向( )的设计方法。', 'A.数据流|B.模块|C.数据结构|D.程序', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (34, 1, '与确认测试阶段有关的文档是( )。', 'A.需求规格说明书|B.概要设计说明书|C.详细设计说明书|D.源程序', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (35, 1, '软件开发的需求活动，其主要任务是（ ）。', 'A.给出软件解决方案|B.给出系统模块结构|C.定义模块算法|D.定义需求并建立系统模型', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (36, 1, '以下说法错误的是( )。', 'A.文档仅仅描述和规定了软件的使用范围及相关的操作命令|B.文档也是软件产品的一部分，没有文档的软件就不成软件|C.软件文档的编制在软件开发工作中占有突出的地位和相当大的工作量|D.高质量文档对于发挥软件产品的效益有着重要的意义', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (37, 1, '一个项目是否开发，从经济上来说是否可行，归根结底是取决于( )。', 'A.成本估算|B.项目计划|C.工程管理|D.工程网络图', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (38, 1, '在面向对象的设计中，我们应遵循的设计准则除了模块化、抽象、低耦合、高内聚以外，还有( )。', 'A.隐藏复杂性|B.信息隐蔽|C.经常类的复用|D.类的开发', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (39, 1, '面向对象的主要特征除对象惟一性、封装、继承外，还有( )。', 'A.多态性|B.完整性|C.可移植性|D.兼容性', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (40, 1, '在考察系统的一些涉及时序和改变的状况时，要用动态模型来表示。动态模型着重于系统的控制逻辑，它包括两个图：一个是事件追踪图，另一个是( )。', 'A.数据流图|B.状态图|C.系统结构图|D.用例图', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (41, 1, '下面说法正确的是( )。', 'A.经过测试没有发现错误说明程序正确|B.测试的目标是为了证明程序没有错误|C.成功的测试是发现了迄今尚未发现的错误的测试|D.成功的测试是没有发现错误的测试', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (42, 1, '( )能够有效地检测输入条件的各种组合可能会引起的错误。', 'A.等价类划分|B.边界值分析|C.错误推测|D.因果图', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (43, 1, '火车是一种陆上交通工具。火车和陆上交通工具之间的关系是( )关系。', 'A.组装|B.整体部分|C.hasa|D.一般特殊', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (44, 1, '软件维护产生的副作用，是指（  ）', 'A、开发时的错误|B、隐含的错误|C、因修改软件而造成的错误|D、运行时误操作', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (45, 1, '使用程序设计的控制结构导出测试用例的测试方法是（    ）', 'A、黑盒测试|B、白盒测试|C、边界测试|D、系统测试', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (46, 1, '软件详细设计的主要任务是确定每个模块的（   ）', 'A、算法和使用的数据结构|B、外部接口|C、功能|D、编程', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (47, 1, '软件结构图的形态特征能反映程序重用率的是（   ）', 'A、深度|B、宽度|C、扇入|D、扇出', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (48, 1, '为了提高模块的独立性，模块内部最好是（    ）', 'A、逻辑内聚|B、时间内聚|C、功能内聚|D、通信内聚', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '数据耦合，功能内聚');
INSERT INTO `questions` VALUES (49, 1, '软件是一种（  ）', 'A、程序|B、数据|C、逻辑产品|D、物理产品', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (50, 1, '需求分析最终结果是产生（   ）', 'A、项目开发计划|B、需求规格说明书|C、设计说明书|D、可行性分析报告', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (51, 1, '因计算机硬件和软件环境的变化而作出的修改软件的过程称为(      )', 'A、纠正性维护|B、适应性维护|C、完善性维护|D、预防性维护', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (52, 1, '下列属于维护阶段的文档是(      )', 'A、软件规格说明|B、用户操作手册|C、软件问题报告|D、软件测试分析报告', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (53, 1, '若有一个计算类型的程序，它的输入量只有一个X，其范围是［-1.0，1.0］，现从输入的角度考虑一组测试用例：-1.001，-1.0，1.0，1.001。设计这组测试用例的方法是(      )', 'A、条件覆盖法|B、等价分类法|C、边界值分析法|D、错误推测法', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (54, 1, '研究开发所需要的成本和资源是属于可行性研究中的(   )研究的一方面。', 'A.技术可行性|B.经济可行性|C.社会可行性|D.法律可行性', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (55, 1, '按软件生命周期方法设计软件的过程中，画数据流图属于下面哪个阶段的工作（）', 'A、需求分析|B、程序设计|C、详细设计|D、软件维护', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (56, 1, '经过严密的软件测试后所提交给用户的软件产品中（  ）', 'A、软件不再包含任何错误|B、还可能包含少量软件错误|C、所提交给用户的可执行文件不会含有错误|D、文档中不会含有错误。', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (57, 1, '等价划分测试方法属于（  ）', 'A、黑盒测试|B、白盒测试|C、边界测试|D、系统测试', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (58, 1, '软件按照设计的要求，在规定时间和条件下达到不出故障，持续运行的要求的质量特性称为(    )', 'A.可用性|B.可靠性|C.正确性|D.完整性', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (59, 1, '确认软件的功能是否与需求规格说明书中所要求的功能相符的测试属于（  ）', 'A、集成测试|B、恢复测试|C、确认/验收测试|D、单元测试', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (60, 1, '要显示描绘软件开发项目各作业的依赖关系，应选择(  )。', 'A.Gantt图|B.工程网络|C.COCOMO模型|D.数据流图', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (61, 1, '从心理学角度看，对数据流程图的数据处理泡进行分解，一次分解为多少个泡为宜。（  ）', 'A、3±1|B、7±2|C、15±1|D、18±2', 'B', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (62, 1, '结构化程序设计主要强调的是（    ）', 'A、程序的规模|B、程序的效率|C、程序设计语言的先进性|D、程序易读性', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (63, 1, '在用户界面层次上对软件进行测试属于哪种测试方法（   ）', 'A、黑盒测试|B、白盒测试|C、边界测试|D、系统测试', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (64, 1, '对象实现了数据和操作的结合， 使数据和操作（    ）于对象的统一体中。', 'A、结合|B、隐藏|C、封装|D、抽象', 'C', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (65, 1, '(  )是比较理想的可重用软构件。', 'A.子程序库|B.源代码包含文件|C.对象|D.类', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '面向对象技术中的“类”,是比较理想的 可重 用的软构件,不妨称之为类构件,');
INSERT INTO `questions` VALUES (66, 1, '下列模型属于成本估算方法的有(      )', 'A、COCOMO模型|B、McCall模型|C、McCabe度量法|D、时间估算法', 'A', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (67, 1, '软件生存周期中时间最长的是（     ）阶段。', 'A、总体设计|B、需求分析|C、软件测试|D、软件维护', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '暂无');
INSERT INTO `questions` VALUES (68, 1, '为了提高模块的独立性，模块之间的联系最好的是（   ）。', 'A、公共耦合|B、控制耦合|C、内容耦合|D、数据耦合', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', '耦合性与内聚性是模块独立性的两个定性标准，是互相关联的。在程序设计中，各模块间的内聚性越强，则耦合性越弱。一般较优秀的软件设计，应尽量做到高内聚、低耦合，有利于提高模块的独立性。');
INSERT INTO `questions` VALUES (69, 1, '采用 Gantt 图表示软件项目进度安排，下列说法中正确的是 ( )  ', 'A，能够反映多个任务之间的复杂关系|B,能够直观表示任务之间相互依赖制约关系|C，能够表示哪些任务是关键任务|D,能够表示子任务之间的并行和串行关系', 'D', '2020-08-21 09:23:20', '2020-08-21 09:23:34', NULL);
INSERT INTO `questions` VALUES (70, 2, '下列软件属性中，软件产品首要满足的应该是（）', 'A．功能需求|B．性能需求|C．可扩展性和灵活性|D．容错纠错能力', 'A', '2020-08-25 03:49:15', '2020-08-25 03:50:03', NULL);
INSERT INTO `questions` VALUES (71, 2, '软件缺陷产生的原因是【】。 ', 'A．交流不充分及沟通不畅；软件需求的变更；软件开发工具的缺陷|B．软件的复杂性；软件项目的时间压力|C．程序开发人员的错误；软件项目文档的缺乏|D．以上都是 ', 'D', '2020-08-25 03:51:02', '2020-08-25 03:51:02', NULL);
INSERT INTO `questions` VALUES (72, 2, '导致软件缺陷的最大原因是【】', 'A．规格说明书|B．设计|C．编码|D．测试 ', 'A', '2020-08-25 05:49:35', '2020-08-25 05:50:31', NULL);
INSERT INTO `questions` VALUES (73, 2, '修复软件缺陷费用最高的是【】阶段', 'A．编制说明书|B．设计|C．编写代码|D．发布 ', 'D', '2020-08-25 05:51:07', '2020-08-25 05:51:07', NULL);
INSERT INTO `questions` VALUES (74, 2, '下列【】不属于软件缺陷。', 'A．测试人员主观认为不合理的地方|B．软件未达到产品说明书标明的功能|C．软件出现了产品说明书指明不会出现的错误|D．软件功能超出产品说明书指明范围', 'A', '2020-08-25 05:51:56', '2020-08-25 05:51:56', NULL);
INSERT INTO `questions` VALUES (75, 2, '软件测试的目的是【】', 'A．避免软件开发中出现的错误|B．发现软件开发中出现的错误|C．尽可能发现并排除软件中潜藏的错误，提高软件的可靠性|D．修改软件中出现的错误 ', 'C', '2020-08-25 05:52:40', '2020-08-25 05:52:40', NULL);
INSERT INTO `questions` VALUES (76, 2, '软件测试技术可以分为静态测试和动态测试，下列说法中错误的是【】 ', 'A．静态测试是指不运行实际程序，通过检查和阅读等手段来发现程序中的错误|B．动态测试是指实际运行程序，通过运行的结果来发现程序中的错误|C．动态测试包括黑盒测试和白盒测试。 |D．白盒测试是静态测试，黑盒测试是动态测试。', 'D', '2020-08-25 06:06:41', '2020-08-25 06:22:18', NULL);
INSERT INTO `questions` VALUES (77, 2, '黑盒测试是根据软件的【】来设计测试用例', 'A．功能|B．规格说明|C．内部逻辑|D．内部数据 ', 'A', '2020-08-25 06:07:41', '2020-08-25 06:26:08', NULL);
INSERT INTO `questions` VALUES (78, 2, '用黑盒技术设计测试用例的方法之一为【】 ', 'A．因果图        |B．逻辑覆盖      |C．循环覆盖      |D．基本路径测试 ', 'A', '2020-08-25 07:19:11', '2020-08-25 07:19:55', NULL);
INSERT INTO `questions` VALUES (79, 2, '坚持在软件的各个阶段实施下列哪种质量保障措施，才能在开发过程中尽早发现和预防错误，把出现的错误克服在早期【】。', '  A．技术评审       |B．程序测试    |C．改正程序错误     |D．管理评审 ', 'D', '2020-08-25 07:29:26', '2020-08-25 07:29:26', NULL);
INSERT INTO `questions` VALUES (80, 2, '为了提高测试的效率，正确的做法是【】。 ', 'A．选择发现错误可能性大的数据作为测试用例 |B．在完成程序的编码之后再制定软件的测试计划 |C．随机选取测试用例 |D．使用测试用例测试是为了检查程序是否做了应该做的事 ', 'A', '2020-08-25 07:30:14', '2020-08-25 07:30:14', NULL);
INSERT INTO `questions` VALUES (81, 2, '对程序的测试最好由【】来做。', '  A．程序员      |B．第三方测试机构      |C．程序开发组      |D．用户  ', 'B', '2020-08-25 07:30:34', '2020-08-25 07:30:34', NULL);
INSERT INTO `questions` VALUES (82, 2, '在边界值分析中，下列数据通常不用来做数据测试的是【】。 ', 'A．正好等于边界的值          |B．等价类中的等价值 |C．刚刚大于边界的值     |D．刚刚小于边界的值 ', 'B', '2020-08-25 07:30:51', '2020-08-25 07:30:51', NULL);
INSERT INTO `questions` VALUES (83, 2, '单元测试中设计测试用例的依据是【】。 ', 'A．概要设计规格说明书         |B．用户需求规格说明书 |C．项目计划说明书              |D．详细设计规格说明书 ', 'D', '2020-08-25 07:31:09', '2020-08-25 07:31:09', NULL);
INSERT INTO `questions` VALUES (84, 2, '如果一个判定中的复合条件表达式为（A > 1）or（B <= 3），则为了达到100%的条件覆盖率，至少需要设计多少个测试用例【】。', '  A．1 |B．2 |C．3 |D．4 ', 'D', '2020-08-25 07:32:02', '2020-08-25 07:32:02', NULL);
INSERT INTO `questions` VALUES (85, 2, '在某大学学籍管理信息系统中，假设学生年龄的输入范围为16—40，则根据黑盒测试中的等价类划分技术，下面划分正确的是【】。 ', 'A．可划分为2个有效等价类，2个无效等价类 |B．可划分为1个有效等价类，2个无效等价类 |C．可划分为2个有效等价类，1个无效等价类 |D．可划分为1个有效等价类，1个无效等价类', 'B', '2020-08-25 07:32:35', '2020-08-25 07:32:35', NULL);
INSERT INTO `questions` VALUES (86, 2, '下面有关测试原则的说法正确的是【】。', 'A．测试用例应由测试的输入数据和预期的输出结果组成 |B．测试用例只需选取合理的输入数据 |C．程序最好由编写该程序的程序员自己来测试  |D．使用测试用例进行测试是为了检查程序是否做了它该做的事 ', 'A', '2020-08-25 07:33:03', '2020-08-25 07:33:03', NULL);
INSERT INTO `questions` VALUES (87, 2, '下列关于测试方法的叙述中不正确的是【】。', '  A．从某种角度上讲，白盒测试与黑盒测试都属于动态测试 |B．功能测试属于黑盒测试 |C．对功能的测试通常是要考虑程序的内部结构 |D．结构测试属于白盒测试 ', 'C', '2020-08-25 07:36:06', '2020-08-25 07:36:06', NULL);
INSERT INTO `questions` VALUES (88, 2, '下列方法中，不属于黑盒测试的是【】。 ', 'A．基本路径测试法        |B．等价类测试法 |C．边界值分析法          |D．基于场景的测试方法 ', 'A', '2020-08-25 07:42:01', '2020-08-25 07:43:18', NULL);
INSERT INTO `questions` VALUES (89, 2, '不属于白盒测试的技术是 【】。', 'A．语句覆盖     |B．判定覆盖  |C．边界值分析 |D．基本路径测试  ', 'C', '2020-08-25 07:42:21', '2020-08-25 07:42:21', NULL);
INSERT INTO `questions` VALUES (90, 2, '测试程序时，（穷举、穷尽）不可能遍历所有可能的输入数据，而只能是选择一个子集进行测试，那么最好的选择方法是【】。 ', 'A．随机选择 |B．划分等价类  |C．根据接口进行选择 |D．根据数据大小进行选择', 'B', '2020-08-25 07:42:42', '2020-08-25 07:42:42', NULL);
INSERT INTO `questions` VALUES (91, 2, '下列可以作为软件测试对象的是【】。', 'A．需求规格说明书|B．软件设计规格说明 |C．源程序|D．以上全部 ', 'D', '2020-08-25 07:44:42', '2020-08-25 07:44:42', NULL);
INSERT INTO `questions` VALUES (92, 2, '在软件测试阶段，测试步骤按次序可以划分为以下几步：【】', ' A．单元测试、集成测试、系统测试、验收测试 |B．验收测试、单元测试、系统测试、集成测试 |C．单元测试、集成测试、验收测试、系统测试 |D．系统测试、单元测试、集成测试、验收测试 ', 'A', '2020-08-25 07:45:03', '2020-08-25 07:45:03', NULL);
INSERT INTO `questions` VALUES (93, 2, '软件测试过程中的集成测试主要是为了发现【】阶段的错误。', 'A.需求分析 |B.概要设计 |C.详细设计 |D.编码', 'B', '2020-08-25 07:46:00', '2020-08-25 07:46:00', NULL);
INSERT INTO `questions` VALUES (94, 2, '下列指导选择和使用测试覆盖率的原则中错误的是【】。', ' A．覆盖率不是目的，仅是一种手段 |B．不要追求绝对100%的覆盖率  |C．不可能针对所有的覆盖率指标来选择测试用例 |D．只根据测试覆盖率指标来指导测试用例的设计 ', 'D', '2020-08-25 07:46:41', '2020-08-25 07:46:41', NULL);
INSERT INTO `questions` VALUES (95, 2, '测试文档种类包括【】。 ', 'A．需求类文档、计划类文档 |B．设计类文档、执行类文档 |C. 缺陷记录类、阶段汇总类 测试总结类 |D．以上都有 ', 'D', '2020-08-25 07:46:59', '2020-08-25 07:46:59', NULL);
INSERT INTO `questions` VALUES (96, 2, '以下关于软件回归测试的说法中错误的是【】。 ', 'A．软件变更后，应对软件变更部分的正确性和对变更需求的符合性进行测试 |B．软件变更后，首先应对变更的软件单元进行测试，然后再进行其他相关的测试  |C．软件变更后，不必再对软件原有正确的功能、性能和其他规定的要求进行测试  |D．对具体的软件，可以根据软件测试合同及软件的重要性、完整性级别对回归测试内容进行剪裁 ', 'C', '2020-08-25 07:48:12', '2020-08-25 07:48:12', NULL);
INSERT INTO `questions` VALUES (97, 2, '以下说法中错误的是【】 ', 'A．软件配置项测试的目的是检验软件配置与软件需求规格说明的一致性 |B．软件配置项测试一般由软件供方组织，由独立于软件开发的人员实施，软件开发人员配合  |C．软件配置项测试不得委托第三方实施  |D．软件配置项测试要求被测软件配置项已通过单元测试和集成测试 ', 'C', '2020-08-25 07:48:36', '2020-08-25 07:48:36', NULL);
INSERT INTO `questions` VALUES (98, 2, '下面说法正确的是 【】。 ', 'A．经过测试没有发现错误说明程序正确 |B．测试的目标是为了证明程序没有错误  |C．成功的测试是发现了迄今尚未发现的错误的测试 |D．成功的测试是没有发现错误的测试  ', 'C', '2020-08-25 07:48:56', '2020-08-25 07:49:09', NULL);
INSERT INTO `questions` VALUES (99, 2, '以下哪种软件测试属于软件性能测试的范畴【】。', 'A．接口测试   |B．压力测试   |C．单元测试   |D．易用性测试', 'B', '2020-08-25 07:50:07', '2020-08-25 07:51:22', NULL);
INSERT INTO `questions` VALUES (100, 2, '软件测试管理包括测试过程管理、配置管理以及【】', 'A．测试评审管理 |B．测试用例管理 |C．测试计划管理  |D．测试实施管理 ', 'D', '2020-08-25 07:50:33', '2020-08-25 07:50:33', NULL);
INSERT INTO `questions` VALUES (101, 2, '【】的目的是对最终软件系统进行全面的测试，确保最终软件系统满足产品需求并且遵循系统设计。', 'A．系统测试       |B．集成测试       |C．单元测试      |D．功能测试 ', 'A', '2020-08-26 03:31:27', '2020-08-26 03:31:27', NULL);
INSERT INTO `questions` VALUES (102, 2, '如果一个产品中次严重的缺陷基本完成修正并通过复测，这个阶段的成品是【】。', 'A．Alpha版 |B．Beta版 |C．正版 |D．以上都不是 ', 'A', '2020-08-26 03:32:02', '2020-08-26 03:32:02', NULL);
INSERT INTO `questions` VALUES (103, 2, '下列工具中可以直接连接mysql的工具有【】。 ', 'A．xsell    |B．plsql      |C．navicat     |D．以上都不是 ', 'C', '2020-08-26 03:32:27', '2020-08-26 03:32:27', NULL);
INSERT INTO `questions` VALUES (104, 2, '必须要求用户参与的测试阶段是【】。 ', 'A．单元测试           |B．集成测试        |C．确认测试      |D．验收测试 ', 'D', '2020-08-26 03:32:48', '2020-08-26 03:32:48', NULL);
INSERT INTO `questions` VALUES (105, 2, '对Web网站进行的测试中，属于功能测试的是【】 ', 'A．连接速度测试         |B．链接测试   |C．平台测试    |D．安全性测试 ', 'B', '2020-08-26 03:33:03', '2020-08-26 03:33:03', NULL);
INSERT INTO `questions` VALUES (106, 2, '【】不是软件自动化测试的优点。 ', 'A．速度快、效率高|B．准确度和精确度高 |C．能提高测试的质量|D．能充分测试软件', 'D', '2020-08-26 03:33:41', '2020-08-26 03:33:41', NULL);
INSERT INTO `questions` VALUES (107, 2, '配置测试是指【】。 ', 'A．检查软件之间是否正确交互和共享信息   |B．交互适应性、实用性和有效性的集中体现  |C．使用各种硬件来测试软件操作的过程     |D．检查缺陷是否有效改正 ', 'B', '2020-08-26 03:34:06', '2020-08-26 03:34:06', NULL);
INSERT INTO `questions` VALUES (108, 2, '下列不属于测试原则的是【】。 ', 'A．软件测试是有风险的行为        |B．完全测试程序是不可能的  |C．测试无法显示潜伏的软件缺陷    |D．找到的缺陷越多软件的缺陷就越少 ', 'D', '2020-08-26 03:34:23', '2020-08-26 03:34:23', NULL);
INSERT INTO `questions` VALUES (109, 2, '下列各项中【】不是一个测试计划所应包含的内容。', 'A．测试资源、进度安排   |B．测试预期输出   |C．测试范围  |D．测试策略 ', 'D', '2020-08-26 03:34:40', '2020-08-26 03:34:40', NULL);
INSERT INTO `questions` VALUES (110, 2, '调试是【】。 ', 'A．发现与预先定义的规格和标准不符合的问题 |B．发现软件错误征兆的过程|C．有计划的、可重复的过程 |D．消除软件错误的过程 ', 'B', '2020-08-26 03:35:01', '2020-08-26 03:35:01', NULL);
INSERT INTO `questions` VALUES (111, 2, '下列描述错误的是【】。', 'A．软件发布后如果发现质量问题，那是软件测试人员的错 |B．穷尽测试实际上在一般情况下是不可行的 |C．软件测试自动化不是万能的  |D．测试能由非开发人员进行，调试必须由开发人员进行。 ', 'A', '2020-08-26 03:35:21', '2020-08-26 03:35:21', NULL);
INSERT INTO `questions` VALUES (112, 2, '在软件修改之后，再次运行以前为发现错误而执行程序曾用过的测试用例，这种测试称之为【】。 ', 'A．单元测试 |B．集成测试 |C．回归测试 |D．验收测试 ', 'C', '2020-08-26 03:35:36', '2020-08-26 03:35:36', NULL);
INSERT INTO `questions` VALUES (113, 2, '在下面所列举中的逻辑测试覆盖中，测试覆盖最强的是【】。', 'A. 条件覆盖 |B．条件组合覆盖    |C．语句覆盖       |D．判定覆盖', 'C', '2020-08-26 03:36:05', '2020-08-26 03:36:05', NULL);
INSERT INTO `questions` VALUES (114, 2, '下列关于Web应用软件测试的说法中，正确的是【】。 ', 'A．Cookie测试是Web应用软件功能测试的重要内容  |B．对于没有使用数据库的Web应用软件，不需要进行性能测试 |C．链接测试是Web应用软件易用性测试的重要内容  |D．Web应用软件安全性测试仅关注Web应用软件是能够防御网络攻击', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (115, 2, '软件质量特性中，可复用性是软件的', 'A．设计特性    |B．修正特性    |C．运行特性    |D．转移特性', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (116, 2, '在整个软件生存期，确认、验证、测试分别有其侧重的阶段，其中测试主要体现在', 'A．编码阶段                  |B．设计阶段和编码阶段  |C．计划阶段和需求分析阶段    |D．需求分析阶段和设计阶段', 'A', '2020-08-26 03:31:27', '2020-08-26 03:31:27', NULL);
INSERT INTO `questions` VALUES (117, 2, '调试技术包括了处理软件缺陷和查看代码的过程，同它表面相似的技术是', 'A．静态黑盒测试    |B．动态黑盒测试    |C．静态白盒测试    |D．动态白盒测试', 'B', '2020-08-26 03:32:02', '2020-08-26 03:32:02', NULL);
INSERT INTO `questions` VALUES (118, 2, '黑盒测试的主要依据是', 'A．项目计划书    |B．设计设计书    |C．程序源代码    |D．产品说明书', 'D', '2020-08-26 03:32:27', '2020-08-26 03:32:27', NULL);
INSERT INTO `questions` VALUES (119, 2, '利用需求说明的描述找出输入/输出存在的问题，而选取的黑盒测试用例设计方法是', 'A．因果图    |B．边界值分析    |C．判定表驱动    |D．等价类划分', 'A', '2020-08-26 03:32:48', '2020-08-26 03:32:48', NULL);
INSERT INTO `questions` VALUES (120, 2, '设学生成绩的输入范围是0—100，则根据等价类划分技术，正确的划分是', 'A．可划分为1个有效等价类和1个无效等价类  |B．可划分为1个有效等价类和2个无效等价类   |C．可划分为2个有效等价类和1个无效等价类 |D．可划分为2个有效等价类和2个无效等价类', 'B', '2020-08-26 03:33:03', '2020-08-26 03:33:03', NULL);
INSERT INTO `questions` VALUES (121, 2, '下列选项，不属于自动化测试所带来的好处是', 'A．回归测试更方便      |B．可运行更多测试  |C．有效避免重复测试    |D．降低测试成本', 'C', '2020-08-26 03:33:41', '2020-08-26 03:33:41', NULL);
INSERT INTO `questions` VALUES (122, 2, '假设每月20日是市话交费的高峰期，全市几千个收费网点同时启动，决策者为了验证系统的承受力，特别需要对系统提前做的是', 'A．并发性测试    |B．兼容性测试    |C．可用性测试    |D．安全眭测试', 'A', '2020-08-26 03:34:06', '2020-08-26 03:34:06', NULL);
INSERT INTO `questions` VALUES (123, 2, '“检查软件在一个特定的硬件、软件、操作系统等环境下是否能够正常地运行，检查软件之间是否能够正确地交互和共享信息，以及检查软件版本之间的使用问题”。这句话所指的是程序的', 'A．容错性    |B．可用性    |C．安全性    |D．兼容性', 'D', '2020-08-26 03:34:23', '2020-08-26 03:34:23', NULL);
INSERT INTO `questions` VALUES (124, 2, '在GUI测试的几个阶段中，和单元测试对应的是', 'A．底层测试    |B．应用测试    |C．集成测试    |D．兼容性测试', 'A', '2020-08-26 03:34:40', '2020-08-26 03:34:40', NULL);
INSERT INTO `questions` VALUES (125, 2, '面向对象集成测试通常需要发生在整个程序', 'A．编辑完成前    |B．编译完成前    |C．编译完成后    |D．运行完成后', 'C', '2020-08-26 03:35:01', '2020-08-26 03:35:01', NULL);
INSERT INTO `questions` VALUES (126, 2, '指导性审查活动中的基本角色，并不包括', 'A．领域专家    |B．最终用户    |C．测试者    |D．开发者', 'B', '2020-08-26 03:35:21', '2020-08-26 03:35:21', NULL);
INSERT INTO `questions` VALUES (127, 2, '在对象交互的类型中，能真实地模拟了问题空间的是', 'A．构造类    |B．协作类    |C．汇集类    |D．原始类', 'D', '2020-08-26 03:35:36', '2020-08-26 03:35:36', NULL);
INSERT INTO `questions` VALUES (128, 2, '可以提供产品测试工作概述的测试文档是', 'A．测试用例    |B．测试计划    |C．测试报告    |D．测试说明', 'B', '2020-08-26 03:36:05', '2020-08-26 03:36:05', NULL);
INSERT INTO `questions` VALUES (129, 2, '测试环境和开发环境隔离后，不会影响到', 'A．环境的变化    |B．版本的管理  |C．人员的培训    |D．操作环境的变化', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (130, 3, '以下哪一项最能表现某个项目的特征()', 'A. 运用进度计划技巧 |B. 整合范围与成本|C. 确定期限 |D. 利用网络进行跟踪', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (131, 3, '项目管理需要在相互间有冲突的要求中寻找平衡，除了:()', 'A. 甲方和乙方的利益 |B. 范围，时间，成本，质量 |C. 有不同需求和期望的项目干系人 |D. 明确的和未明确表达的需求', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (132, 3, '以下都是日常运作和项目的共同之处，除了:()', 'A. 由人来作 |B. 受制于有限的资源|C. 需要规划、执行和控制 |D. 都是重复性工作', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (133, 3, '有效的项目管理要求项目管理团队理解和利用以下专业知识领域的知识和技能，除了:()', 'A. 项目管理知识体系 |B. 应用领域知识、标准与规章制度|C. 以项目为手段对日常运作进行管理 |D. 处理人际关系技能', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (134, 3, '下列都是子项目的正确说法，除了:()', 'A. 划分子项目的目的是为了便于管理 |B. 子项目的划分便于发包给其他单位|C. 项目生命期的一个阶段是子项目 |D. 子项目不能再往下划分成更小的子项目 ', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (135, 3, '管理一个项目包括以下内容，除了:()', 'A. 识别要求|B. 确定清楚而又能实现的目标 |C. 权衡质量、范围、时间和费用的要求|D. 制定符合项目经理期望的计划和说明书 ', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (136, 3, '()是为创造一种产品、服务或者结果而进行的临时性的努力', 'A. 项目群|B. 过程|C. 项目 |D. 组合', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (137, 3, '下列()不属于项目管理的特征', 'A. 独特性 |B. 通过渐进性协助实施的 |C. 拥有主要顾客或项目发起人 |D. 具有很小的确定性', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (138, 3, '下列()不属于项目管理的三维约束', 'A. 达到范围目标 |B. 达到时间目标|C. 达到沟通目标|D. 达到成本目标', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (139, 3, '()就是将知识、技能、工具和技术应用到项目活动，以达到组织的要求', 'A. 项目管理 |B. 项目组管理|C. 项目组合管理|D. 需求管理 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (140, 3, '为相同的功能组合而实施的一系列应用开发项目，作为()的一部分，后者能够得到更好的管理。', 'A. 组合 |B. 项目群|C. 投资|D. 合作', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (141, 3, 'PMI提供的认证项目叫做()', 'A. Microsoft Certified Project Manager |B. Project Management Professional|C. Project Management Expert |D. Project Management Menter ', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (142, 3, '乙方在项目初始阶段的主要任务不包含以下哪一项()', 'A.项目分析 |B.竞标|C.合同签署 |D.合同管理', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (143, 3, '项目章程中不包括以下哪项内容()', 'A. 项目成本估算 |B. 项目名称|C. 项目经理及联系方式|D. 项目成员及角色', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (144, 3, '项目经理的职责不包括以下哪项内容()', 'A. 开发计划|B. 组织实施|C. 项目控制|D. 系统支持', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (145, 3, '项目经理的权利不包括以下哪一项()', 'A. 制定决策 |B. 项目控制|C. 挑选项目成员 |D. 资源分配</strong>', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (146, 3, '为避免一次性投资太多带来的风险，最好选择()生存期模型，', 'A. 增量式模型 |B. 原型|C. 螺旋模型|D. V模型', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (147, 3, '可以构建一部分系统的模型，通过用户试用提出优缺点，最好选择()生存期模型，', 'A. 增量式模型 |B. 原型 |C. 螺旋模型|D. V模型', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (148, 3, '项目章程()', 'A. 明确了项目经理 |B. 确定了项目的质量标准 |C. 明确了团队的纪律|D. 定义了项目需求', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (149, 3, '对于风险比较大的项目，最好选择()生存期模型 ', 'A. 瀑布模型|B. 原型 |C. 螺旋模型 |D. V模型', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (150, 3, '开发项目建议书的目的是为了()', 'A. 验收|B. 竞标或者签署合同|C. 编写计划|D. 跟踪控制项目', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (151, 3, '项目建议书是哪个阶段开发的文档()', 'A. 项目执行阶段 |B. 项目结尾阶段 |C. 项目初始阶段 |D. 项目计划阶段 ', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (152, 3, '需求分析是回答系统必须()的问题', 'A. 做什么|B. 怎么做|C. 何时做 |D. 为谁做 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (153, 3, 'WBS()', 'A. 帮助组织工作|B. 防止遗漏工作|C. 为项目估算提供依据|D. 确定团队成员责任', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (154, 3, '项目范围()', 'A. 只在项目开始时重要|B. 在授权项目的合同或者其他文件得以批准后就不再重要了|C. 从项目概念阶段到收尾阶段都应该加以管理和控制|D. 是在项目执行阶段通过变更控制步骤进行处理的问题', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (155, 3, '为了有效地管理项目，应该将工作分解为更小的部分，以下各项中，哪一项不能说明任务应该分解到什么程度？()', 'A. 可以在80小时内完成 |B. 不能再进一步进行逻辑细分了|C. 可由一个人完成|D. 可以进行实际估算 ', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (156, 3, '范围变更是指()', 'A. 修改技术规格|B. 对范围陈述进行修订|C. 对批准后的WBS进行修改 |D. 以上都不是 ', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (157, 3, '下面哪个不是需求管理的过程()', 'A. 需求设计|B. 需求获取|C. 需求分析|D. 需求变更', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (158, 3, '下面那个不是创建WBS的方法()', 'A. 自顶向下|B. 自底向上 |C. 控制方法 |D. 模版指导', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (159, 3, '任务分解可以()，它是范围变更的一项重要输入', 'A. 提供项目成本估算结果|B. 提供项目范围基线 |C. 规定项目采用的过程 |D. 提供项目的关键路径', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (160, 3, '范围基线由()组成', 'A. 项目章程、批准的详细的项目范围说明书和WBS|B. 批准的详细项目范围说明书、WBS和WBS字典|C. 项目章程、项目工作说明书和WBS|D. WBS', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (161, 3, '以下哪项是指对项目包括什么与不包括什么的定义与控制过程。()', 'A. 项目章程|B. 投标书 |C. 项目范围管理|D. 项目成本估算', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (162, 3, '()反映了组织机构或客户对系统、产品高层次的目标需求，有管理人员或市场分人员确定。', 'A. 业务需求 |B. 质量需求|C. 范围定义|D. 功能需求', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (163, 3, '()描述了用户通过使用本软件产品必须要完成的任务，一般是用户协助提供。', 'A. 约束和假设 |B. 非功能性需求|C. 功能需求 |D. 用户需求', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (164, 3, '()定义了开发人员必须实现的软件功能，使得用户通过使用此软件能完成他们的任务，从而满足了业务需求。', 'A. 功能需求 |B. 系统需求 |C. 质量特性 |D. 业务需求', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (165, 3, '()是对需求进行调查、收集、分析、评价、定义等所有活动。', 'A. 需求获取 |B. 需求管理 |C. 需求开发 |D. 需求工程', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (166, 3, '需求分析完成的标志是()', 'A. 开发出初步原型 |B. 提交一份工作陈述 |C. 提交项目章程 |D. 提交一份完整的软件需求规格说明书', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (167, 3, '()是软件项目的一个突出的特点，也是软件项目最为普遍的一个特点。', 'A. 需求变更 |B. 暂时性|C. 阶段性 |D. 约束性', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (168, 3, 'WBS中的每一个具体细目通常都指定唯一的()', 'A. 编码|B. 责任人 |C. 功能模块|D. 提交截至期限', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (169, 3, '任务分解时，()方法从一般到特殊的方向进行，从项目的大局着手，然后逐步分解子细目，将项目变为更细更完善的部分。', 'A. 模板参照 |B. 自顶向下 |C. 类比 |D. 自底向上', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (170, 3, '任务分解时，()方法从特殊到一般的方向进行，首先定义一些特殊的任务，然后将这些任务组织起来，形成更高级别的WBS层。', 'A. 模板|B. 自顶向下 |C. 类比 |D. 自底向上', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (171, 3, '快速跟进是指()', 'A. 采用并行执行任务，加速项目进展 |B. 用一个任务取代另外的任务|C. 如有可能，减少任务数量 |D. 减轻项目风险', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (172, 3, '赶工一个任务时，你应该关注()', 'A. 尽可能多的任务|B. 非关键任务|C. 加速执行关键路径上的任务|D. 通过成本最低化加速执行任务', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (173, 3, '“软件编码完成之后，我才可以对它进行软件测试”,这句话说明了哪种依赖关系？()', 'A. 强制性依赖关系|B. 软逻辑关系 |C. 外部依赖关系 |D. 里程碑', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (174, 3, '如果用户提供的环境设备需要５月１０日到位，所以环境测试安排在５月１０日以后，这种活动安排的依赖依据是：()', 'A. 强制性依赖关系 |B. 软逻辑关系|C. 外部依赖关系 |D. 里程碑', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (175, 3, '对一个任务进行进度估算时,A是乐观者,估计是6天完成,B是悲观者,估计是24天完成,C是有经验者认为最有可能是12天完成，那么这个任务的历时估算是介于10天到16天的概率是()', 'A. 50%|B. 68.3% |C. 70% |D. 99.7%', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (176, 3, '下面哪项可能延长项目的进度？()', 'A. Lag |B. Lead |C. 赶工|D. 快速跟进 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (177, 3, '关于浮动，下面哪个是正确的？()', 'A. 每个任务都有浮动 |B. 只有复杂的项目有浮动|C. 浮动是在不增加项目成本的条件下，一个活动可以延迟的时间量|D. 浮动是在不影响项目完成时间的前提下，一个活动可以延迟的时间量', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (178, 3, '关于网络图，下面哪个是不正确的？()', 'A. 网络图可用于安排计划 |B. 网络图展示任务之间的逻辑关系|C. 网络图可用于跟踪项目 |D. 网络图可用于详细的时间管理', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (179, 3, '资源平衡最好用于()活动', 'A. 时间很紧的|B. 按时的 |C. 非关键路径 |D. 关键路径 ', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (180, 3, '下面哪项可以决定进度的灵活性()', 'A. PERT |B. Total float|C. ADM|D. 赶工', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (181, 3, '()可以显示任务的基本信息，使用甘特图能方便地查看任务的工期，开始和结束时间以及资源的信息。', 'A. 甘特图 |B. 网络图 |C. 里程碑图|D. 资源图 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (182, 3, '()是用系统的功能数量来测量其规模，与实现产品所使用的语言和技术没有关系的。', 'A. 功能点|B. 对象点|C. 代码行 |D. 用例点', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (183, 3, '如果你是某项目的项目经理，你已经估算出每个任务()', 'A. 自下而上估算法 |B. 类比估算法|C. 专家估算法 |D. 参数估算法', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (184, 3, '在项目初期，进行竞标合同的时候，一般采用的成本估算方法是()', 'A. 参数估算法 |B. 类比估算法|C. 专家估算法 |D. 功能点估算', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (185, 3, '在成本管理过程中，项目经理确定的每个时间段，各个工作单元的成本是()', 'A. 估算|B. 预算|C. 直接成本 |D. 间接成本', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (186, 3, '成本管理就是确保项目在预算范围之内的管理过程，不包括以下哪一项()', 'A. 成本估算 |B. 成本预算 |C. 成本控制 |D. 成本核算', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (187, 3, '成本估算的输入一般不包括以下哪一项？()', 'A. 需求或者WBS |B. 资源需求和消耗率|C. 进度规划|D. 质量标准', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (188, 3, '估算文件不包括以下哪一项()', 'A. 功能模块|B. 质量标准|C. 资源及数量 |D. 估算成本', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (189, 3, '常见的成本估算方法不包括哪一项()', 'A. 代码行|B. 功能点 |C. 类比法 |D. 关键路径法', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (190, 3, '在项目执行和收尾过程中主要矛盾来自是什么?()', 'A. 进度计划|B. 技术问题 |C. 个性 |D. 质量', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (191, 3, '自下而上的估算除了成本估算外，还属于哪个过程的工具:()', 'A.活动定义|B.活动排序|C.活动资源估算 |D.活动所需时间估算 ', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (192, 3, '项目质量管理的最终责任由谁来承担()', 'A. 项目开发人员 |B. 采购经理|C. 质量经理|D. 项目经理', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (193, 3, '“质量成本”是一个项目管理概念，它说明了下列哪项成本()', 'A. 额外需求的成本 |B. 需求变更的成本|C. 确保符合需求的成本 |D. 固定成本', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (194, 3, '增加有益的活动过程减少没有价值的活动过程是哪类质量活动()', 'A. 质量保证|B. 质量规划|C. 质量控制 |D. 质量改进 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (195, 3, '质量控制是()', 'A. 对每个工作包增加工作时间 |B. 项目生存期的各个阶段都需要实施的 |C. 只需要做一次 |D. 只有大的项目才需要的', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (196, 3, '质量管理计划与质量体系得区别在于()', 'A. 质量计划是针对单一的产品、项目、服务和合同制定的|B. 质量管理系统是针对单一的产品、项目、服务和合同制定的|C. 质量管理系统是由一个单独的组织实体使用|D. 质量计划不属于项目计划的一部分', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (197, 3, '项目质量管理的目标是满足()的需要', 'A. 老板|B. 干系人 |C. 项目|D. 组织 ', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (198, 3, '质量与等级的区别是什么()', 'A. 质量是对需求的满足程度，而等级是对质量的排序。|B. 质量是对需求的满足程度，而等级是对货物和服务的排序。|C. 在项目管理过程中，质量和等级没有明显的区别。|D. 质量是项目的最终结果，而等级是项目进展过程中对质量的排序。', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (199, 3, '下面哪项是质量计划的方法()', 'A. 质量检查|B. 对等评审|C. 抽样分析|D. 试验设计', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (200, 3, '()是企业的生命也是信誉。', 'A. 形象 |B. 质量|C. 利润 |D. 效益', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (201, 3, '质量管理过程的目的是确保项目满足需要执行的过程。主要过程不包含以下哪一项()', 'A. 质量评审 |B. 质量保证|C. 质量规划 |D. 质量控制 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (202, 3, '矩阵型组织的一个主要的优点是()', 'A. 加强项目经理对资源的控制 |B. 项目团队可以有多个老板 |C. 沟通更加便捷 |D. 报告更加容易', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (203, 3, '在哪种组织结构中，项目成员没有安全感()', 'A. 职能型|B. 矩阵型|C. 项目型 |D. 弱矩阵型', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (204, 3, '项目经理在一个高科技公司，现在正在为一个新的项目选择合适的组织结构，这个项目涉及很多的领域和特性，他应该选择哪种组织结构()', 'A. 矩阵型|B. 项目型 |C. 职能型|D. 组织型', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (205, 3, '责任分配矩阵是()', 'A. 进行人力资源计划的一个工具|B. 一种组织结构 |C. 与WBS类似|D. 估算成本的方法 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (206, 3, '人力资源分配图是()', 'A. 展现目前的人力资源质量|B. 展现项目中人力资源在各个阶段的分布情况 |C. 说明人员分工情况|D. 说明项目需要的所有资源 ', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (207, 3, '人员管理计划描述了()', 'A. 如何获取项目成员|B. 如何解决冲突|C. 项目经理的团队建设总结 |D. 项目团队的人员什么时候如何加入到团队中和离开团队', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (208, 3, '项目管理中的三种组织结构，哪一种沟通最为复杂()', 'A. 矩阵型|B. 项目型 |C. 职能型 |D. 都一样 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (209, 3, '项目管理中的三种组织结构，哪一种在项目收尾时，团队成员和项目经理压力比较大()', 'A. 矩阵型|B. 项目型|C. 职能型|D. 都一样', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (210, 3, '项目管理中的三种组织结构，哪一种组织结构是目前最普遍的项目组织形式，它是一个标准的金字塔型组织形式()', 'A. 矩阵型 |B. 项目型 |C. 职能型 |D. 都一样', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (211, 3, '项目管理中的三种组织结构，哪一种组织结构适用于主要由一个部门完成的项目或技术比较成熟的项目()', 'A. 矩阵型|B. 项目型|C. 职能型|D. 都一样 ', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (212, 3, '某项目经理刚刚得知卖方增加了成本，项目经理首先应该确定是否()', 'A. 有足够的储备处理这个变更|B. 另外的卖方可以按照原来的成本完成项目|C. 另外的项目能够节约预算|D. 任务是关键的项目任务', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (213, 3, '可以在()阶段采用Make-or-buy决策分析', 'A. 卖方选择|B. 立项（采购计划编制）|C. 合同管理 |D. 成本计划', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (214, 3, '合同激励的最终目标是()', 'A. 卖方节约成本 |B. 买方节约成本|C. 增加卖方成本 |D. 协调合同双方的目标', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (215, 3, '买卖双方之间存在的法律合同关系称为()', 'A. 合同条款 |B. 合约 |C. 合同当事人 |D. 其他', 'C', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (216, 3, '哪种合同类型，卖方承担的风险最大()', 'A. Cost plus percentage of costs:成本加成本百分比 |B. Cost plus fixed fee:成本加固定费|C. Cost Plus Incentive Fee:成本加奖金 |D. Firm Fixed Price ：固定总价', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (217, 3, '下面哪项与计划签署合同没有关系？()', 'A. 设计模版 |B. 协议附件|C. 招标文件|D. 评估标准 ', 'A', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (218, 3, '合同一旦签署了就具有法律约束力，除非()', 'A. 一方不愿意履行义务|B. 合同违反了法律 |C. 一方宣布合同无效 |D. 一方没有能力负担财务结果 ', 'B', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (219, 3, '在下列当中，对承包商风险最高的合同种类是:()', 'A.成本加奖金 |B.固定价格加奖金 |C.成本加固定价格|D.固定价格，固定总价', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (220, 3, '规定按合同提供产品或服务的文件称作()', 'A.材料帐单|B.项目计划|C.工作描述|D.项目批准书', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);
INSERT INTO `questions` VALUES (221, 3, '下列哪项不是合同的基本要素?()', 'A.报价 |B.接受 |C.订约要因|D.价格结构 ', 'D', '2020-08-26 03:36:25', '2020-08-26 03:36:25', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'wxfeng', '837090181@qq.com', NULL, '$2y$10$0cpluMoNC7pqGQvd278GZuWCP2oLfVstZo/BXsAzm6tTicpWcEkiO', NULL, '2020-07-23 08:45:32', '2020-07-23 08:45:32');

SET FOREIGN_KEY_CHECKS = 1;

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::post('/submit', 'IndexController@submit');

Route::get('/course/{id}', 'CourseController@index')->where('id', '[0-9]+');
Route::get('/question/{id}/{cateid}', 'CourseController@question')->where(['id'=>'[0-9]+','cateid'=>'[0-9]+']);
Route::get('/answer/{id}', 'IndexController@answer')->where('id', '[0-9]+');
//Route::get('/answer/{id}', function ($id) {
//    return 'User '.$id;
//});
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

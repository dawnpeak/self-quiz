@extends('layouts.app')

@section('content')
    <style>
        .breadcrumb, .breadcrumb:before {
            color: #00a157;
            font-size: 16px;
        }

        .breadcrumb:last-child {
            color: #00aa9a;
            font-size: 16px;
        }
    </style>
    <div class="row" style="padding:20px;">
        <div class="col s12">
            <a href="/" class="breadcrumb">首页</a>
            <a href="/course/{{$courseid}}" class="breadcrumb">{{$coursename}}</a>
            <a href="#!" class="breadcrumb">{{$catename}}</a>
        </div>
        <div id="content" class="col s12">
            @if($count)
                <div id="examination">
                    @foreach ($contents as $content)
                        <input type="hidden" id="cateid" value="{{$content->type}}">
                        {{--选择--}}
                        @if($content->type ==1)
                            <blockquote>
                                <span id="question" class="flow-text" name="{{$content->id}}"> {{ $content->question}}</span>
                            </blockquote>
                            @foreach ($content->opts as $key=>$value)
                                <p>
                                    <input name="option" type="radio" id="{{$content->opt_index[$key]}}"/>
                                    <label for="{{$content->opt_index[$key]}}">{{$value}}</label>
                                </p>
                            @endforeach
                        @endif

                        {{--填空--}}
                        @if($content->type ==2)
                            <blockquote>
                                <span id="question" class="flow-text"
                                      name="{{$content->id}}"> {{ $content->question}}</span>
                            </blockquote>
                            <div class="col s12">
                                <div class="input-field col s4">
                                    <input placeholder="请输入问题答案" id="answer" type="text" class="validate">
                                    <label class="" for="answer">答案</label>
                                </div>
                            </div>
                        @endif
                        {{--简答题--}}
                        @if($content->type ==3)
                            <blockquote>
                                <span id="question" class="flow-text" name="{{$content->id}}"> {{ $content->question}}</span>
                            </blockquote>
                        @endif
                    @endforeach
                </div>
                <div id="action" class="center-align">
                    <button class="btn waves-effect waves-light" id="submit">提交
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            @else
                <div class="center-align">
                    <h2>): </h2>
                    <p class="flow-text">暂无相关试题资源</p>
                </div>
            @endif
        </div>
    </div>
    {{$contents->links()}}
@endsection


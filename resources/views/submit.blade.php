<div id="result" style="margin-top: 30px;">
    <blockquote>
        @if ($answer == trim($info['answer']))
            <div class="answer green-text h3">回答正确</div>
        @else
            <div class="answer red-text h3">回答错误</div>
            <div class="answer green-text h3">正确答案：{{$info['answer']}}</div>
            <div class="answer">答案解析：{{$info['analysis']}}</div>
        @endif
    </blockquote>
</div>
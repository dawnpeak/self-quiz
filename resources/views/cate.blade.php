@extends('layouts.app')

@section('content')
    <style>
        .breadcrumb, .breadcrumb:before {
            color: #00a157;
            font-size: 16px;
        }

        .breadcrumb:last-child {
            color: #00aa9a;
            font-size: 16px;
        }

        .cate {
            background: #00aa9a;
            color: #ffffff;
            font-size: 18px;
            font-weight: bold;
            text-align: center;
            margin-top: 30px;
            height:80px;
            line-height: 80px;
            cursor: pointer;
            border: 1px solid #00aa9a;
            border-radius: 5px;
        }
    </style>
    <div class="row" style="padding:20px;">
        <div class="col s12">
            <a href="/" class="breadcrumb">首页</a>
            <a href="#!" class="breadcrumb">{{$coursename}}</a>
        </div>
        <a href="/question/{{$courseid}}/1" class="col s2 offset-s2 cate">单 选 题</a>
        <a href="/question/{{$courseid}}/2" class="col s2 offset-s1 cate">填 空 题</a>
        <a href="/question/{{$courseid}}/3" class="col s2 offset-s1 cate">简 答 题</a>
    </div>
@endsection


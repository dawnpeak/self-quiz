<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    <title>自考刷题库 自测|软件工程自考试题测验|软件工程刷题库</title>
    <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }
        main {
            flex: 1 0 auto;
        }
        .answer {
            padding: 6px;
            font-size: 18px;
        }
    </style>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="/materialize/css/materialize.min.css">
    <link href="/materialize/css/fonts.css" rel="stylesheet">
    <!-- Compiled and minified JavaScript -->
    <script src="/js/jquery-3.5.1.js"></script>
    <script src="/materialize/js/materialize.min.js"></script>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?364e4048ff62d3b235d18f9fe94f616d";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>

<body>
    <header>
        <div id="red" class="block red lighten-1">
            <nav class="pushpin-demo-nav" data-target="red">
                <div class="nav-wrapper  red lighten-1">
                    <div class="container">
                        <a href="/" class="brand-logo">自考刷题库</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">
                            @foreach ($courses as $key => $value)
                            <li><a href="/course/{{$value['id']}}">{{$value['name']}}</a></li>
                            @endforeach
                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            @foreach ($courses as $key => $value)
                            <li><a href="/course/{{$value['id']}}">{{$value['name']}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <main class="container">
        @yield('content')
    </main>
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l4 s12">
                    <h5 class="white-text">版权声明</h5>
                    <p class="grey-text text-lighten-4">网站试题资源均来源于网络，如发现试题答案错误或本站资源侵害到您的权益，请联系管理员进行修改或删除。<br>管理员邮箱：837090181@qq.com
                    </p>
                </div>
                <div class="col l3 offset-l1 s12">
                    <h5 class="white-text">关于网站</h5>
                    <p class="grey-text text-lighten-4">网站内容目前只针对软件工程专业的自考考生，查看答案无需注册，无需付费。由于个人精力有限，试题资源还不丰富。如您有好的试题资源，欢迎推荐。<br>管理员邮箱：837090181@qq.com
                    </p>
                </div>
                <div class="col l3 offset-l1 s12">
                    <h5 class="white-text">友情链接</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" target="_blank" href="http://zkwb.heao.gov.cn/zkservice/default.aspx">河南省自考服务平台</a></li>
                        <li><a class="grey-text text-lighten-3" target="_blank" href="http://www.wangxiaofeng.site/softbook.html">自考教材</a></li>
                        <li><a class="grey-text text-lighten-3" target="_blank" href="http://www.wangxiaofeng.site">我的博客</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2020 Copyright wxfeng  <a target="_blank" href="https://beian.miit.gov.cn/">豫ICP备16037554号</a>
                <a class="grey-text text-lighten-4 right" href="#!">更多链接</a>
            </div>
        </div>
    </footer>
    <script type="application/javascript">
        $(function() {
            $(".button-collapse").sideNav();
            $("#action").on("click", "#submit", function() {
                let id = $('#question').attr("name");
                let cateid = $('#cateid').val();
                let answer = false
                console.log(cateid)
                if (cateid == 1) {
                    answer = $('input:radio:checked').attr("id");
                    if (!answer) {
                        var toastContent = $('<span>请选择答案</span>');
                        Materialize.toast(toastContent, 4000);
                        return false;
                    }
                }
                if (cateid == 2) {
                    answer = $('#answer').val();
                    if (!answer) {
                        var toastContent = $('<span>请填写答案</span>');
                        Materialize.toast(toastContent, 4000);
                        return false;
                    }
                }

                $.ajax({
                    type: "POST",
                    url: "/submit",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    data: {
                        id: id,
                        answer: answer
                    },
                    //返回数据的格式
                    datatype: "html", //"xml", "html", "script", "json", "jsonp", "text".
                    beforeSend: function() {
                        // 禁用按钮防止重复提交
                        $("#submit").attr({
                            disabled: "disabled"
                        });
                    },
                    //成功返回之后调用的函数
                    success: function(data) {
                        $("#result").remove();
                        $("#content").append(decodeURI(data));
                    },
                    //调用执行后调用的函数
                    complete: function() {
                        $("#submit").removeAttr("disabled");
                    }
                });
            });
        });
    </script>
</body>
</html>
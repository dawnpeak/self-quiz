@if ($paginator->hasPages())
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled"><a>上一题</a></li>
            @else
                <li class="waves-effect">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev">上一题</a>
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="waves-effect">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next">下一题</a>
                </li>
            @else
                <li class="disabled" aria-disabled="true"><a>下一题</a></li>
            @endif
        </ul>
@endif

@extends('layouts.app')

@section('content')
    <style>
        .breadcrumb, .breadcrumb:before {
            color: #00a157;
            font-size: 16px;
        }

        .breadcrumb:last-child {
            color: #00aa9a;
            font-size: 16px;
        }
    </style>
    <div class="row" style="padding:20px;">
        <div id="content" class="col s12">
            <div id="examination">
                @foreach ($contents as $content)
                    <blockquote>
                        <input type="hidden" id="cateid" value="{{$content->type}}">
                        <span id="question" class="flow-text"
                              name="{{$content->id}}"> {{ $content->question}}</span>
                    </blockquote>
                    @foreach ($content->opts as $key=>$value)
                        <p>
                            <input name="option" type="radio" id="{{$content->opt_index[$key]}}"/>
                            <label for="{{$content->opt_index[$key]}}">{{$value}}</label>
                        </p>
                    @endforeach
                @endforeach
            </div>

            <div id="action" class="center-align">
                <button class="btn waves-effect waves-light" id="submit">提交
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
    </div>
        {{$contents->links()}}
@endsection


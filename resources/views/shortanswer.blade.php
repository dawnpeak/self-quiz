@extends('layouts.app')

@section('content')
<style>
    .breadcrumb,
    .breadcrumb:before {
        color: #00a157;
        font-size: 16px;
    }

    .breadcrumb:last-child {
        color: #00aa9a;
        font-size: 16px;
    }
</style>
<div class="row" style="padding:20px;">
    <div class="col s12">
        <a href="/" class="breadcrumb">首页</a>
        <a href="/course/{{$courseid}}" class="breadcrumb">{{$coursename}}</a>
        <a href="#!" class="breadcrumb">{{$catename}}</a>
    </div>
    <div id="content" class="col s12">
        @if($count)
        <div id="examination">
            @foreach ($contents as $content)
            <blockquote>
                <span id="question" class="flow-text" name="{{$content->id}}"> {{ $content->question}}</span>
            </blockquote>
            @endforeach
        </div>
        <div id="view" style="margin-top: 30px;">
            <blockquote>
                <a class="waves-effect waves-light btn"><i class="material-icons left">visibility</i>查看答案</a>
            </blockquote>
        </div>
        <div id="result" style="margin-top: 30px;display:none;">
            <blockquote>
                <div class="answer green-text h3">答案：{{$content->answer}}</div>
            </blockquote>
        </div>
        @else
        <div class="center-align">
            <h2>): </h2>
            <p class="flow-text">暂无相关试题资源</p>
        </div>
        @endif
    </div>
</div>
<script type="application/javascript">
    $(function() {
        $("#view").on("click", function() {
            $("#view").hide();
            $("#result").show();
        });
    });
</script>
{{$contents->links()}}
@endsection
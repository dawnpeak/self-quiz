<?php

return [
    'Course id'  => '课程',
    'Question'   => '问题',
    'Type'       => '题目类型',
    'Options'    => '选项',
    'Answer'     => '答案',
    'Analysis'   => '答案解析',
    'Created at' => '创建时间',
    'Updated at' => '更新时间'
];
